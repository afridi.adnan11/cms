(function() {
    var supportTouch = $.support.touch,
        scrollEvent = "touchmove scroll",
        touchStartEvent = supportTouch ? "touchstart" : "mousedown",
        touchStopEvent = supportTouch ? "touchend" : "mouseup",
        touchMoveEvent = supportTouch ? "touchmove" : "mousemove";
    $.event.special.swipeupdown = {
      setup: function() {
        var thisObject = this;
        var $this = $(thisObject);
        $this.bind(touchStartEvent, function(event) {
          var data = event.originalEvent.touches ?
            event.originalEvent.touches[ 0 ] :
            event,
            start = {
                time: (new Date).getTime(),
                coords: [ data.pageX, data.pageY ],
                origin: $(event.target)
            },
            stop;

          function moveHandler(event) {
              if (!start) {
                  return;
              }
              var data = event.originalEvent.touches ?
                      event.originalEvent.touches[ 0 ] :
                      event;
              stop = {
                  time: (new Date).getTime(),
                  coords: [ data.pageX, data.pageY ]
              };

              // prevent scrolling
              if (Math.abs(start.coords[1] - stop.coords[1]) > 10) {
                  event.preventDefault();
              }
          }
          $this
            .bind(touchMoveEvent, moveHandler)
            .one(touchStopEvent, function(event) {
              $this.unbind(touchMoveEvent, moveHandler);
              if (start && stop) {
                if (stop.time - start.time < 1000 &&
                  Math.abs(start.coords[1] - stop.coords[1]) > 30 &&
                  Math.abs(start.coords[0] - stop.coords[0]) < 75) {
                    start.origin
                    .trigger("swipeupdown")
                    .trigger(start.coords[1] > stop.coords[1] ? "swipeup" : "swipedown");
                }
              }
            start = stop = undefined;
          });
        });
      }
    };
    $.each({
      swipedown: "swipeupdown",
      swipeup: "swipeupdown"
    }, function(event, sourceEvent){
      $.event.special[event] = {
        setup: function(){
          $(this).bind(sourceEvent, $.noop);
        }
      };
    });

})();

var w, h;

var t = 700;

var $win    = $(window),
    $body   = $('body'),
    $header = $('.header'),
    $footer = $('.footer');

var $nav = {
    menus    : $('.menu-inner'),
    menu     : $('.menu'),
    mlink    : $('.menu-link.mobile-menu-link'),
    logo     : $('svg.header-logo path'),
    link     : $('.menu-link'),
    controls : $('.control-link'),
    dlink    : $('.menu-link.desktop-menu-link'),
    close    : $('.menu-close'),
    dclose   : $('.menu-desktop-close'),
    item     : $('.menu-item'),
    label    : $('.menu-label'),
    drop     : $('.menu-item-container'),
    brand    : $('.brand-column-menu li'),
    image    : $('.brand-image'),
    brands   : $('.brand-column-menu'),
};

var _global = {
  elem: function() {
    if ($('.error-404').length) {
      var res = (w >= 1024) ? 'desktop' : 'mobile';
      $('.error-404').css({
        'background': 'url(' + $('.error-404').data(res) + ') center no-repeat',
        'background-size': 'cover',
      });
    }
  },

  init: function() {
    $header.removeClass('off');

    $body.removeClass('initializing');

    setTimeout(function() {
      $body.removeClass('switch');
    }, 2000);
  },

  lazy: function() {
    $nav.brand.each(function() {
      $('<img/>')[0].src = $(this).data('image');
    });
  },

  isEmptyObject: function(obj) {
    for(var prop in obj) {
      if (Object.prototype.hasOwnProperty.call(obj, prop)) {
        return false;
      }
    }
    return true;
  },

  rand: function(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);

    return Math.floor(Math.random() * (max - min)) + min;
  },

  res: function() {
    w = $win.width();
    h = $win.height();

    if ($('.grid-product-image-container').length) {
      $('.grid-product-image-container').height($('.grid-product-image-container').width() * .9);
    }
  },

  email: function(email) {
    pattern = /^[A-Z0-9._%+-]+@([A-Z0-9-]+\.)+[A-Z]{2,12}$/i;
    return pattern.test(email);
  },
};

var _nav = {
  inFooter  : false,
  lastPos   : $(document).scrollTop(),
  direction : 'down',
  dirLength : 0,
  threshold : 30,

  check: function() {
    _nav.inFooter = $(document).scrollTop() >= $(document).height() - $footer.height() - _nav.threshold;

    if (_nav.inFooter) {
      _nav.color(innerColor.text, innerColor.logo);
    } else {
      _nav.color(outerColor.text, outerColor.logo);
    }

    var newDir = $(document).scrollTop() > _nav.lastPos ? 'down' : 'up';
    if (newDir == _nav.direction) {
      _nav.dirLength++;
    } else {
      _nav.dirLength = 0;
    }

    if ($(document).scrollTop() > _nav.lastPos && !_nav.inFooter) {
      if (_nav.dirLength >= _nav.threshold) {
        $header.addClass('off');
        $('.product-header').removeClass('header-active');
      }
    } else {
      if (_nav.dirLength >= _nav.threshold || $(document).scrollTop() < 80) {
        $header.removeClass('off');
        $('.product-header').addClass('header-active');
      }
    }

    _nav.direction  = newDir;
    _nav.lastPos    = $(document).scrollTop();
  },

  color: function(items, logo) {
    if (items) {
      logo = logo ? logo : items;
    } else {
      items = outerColor.text;
      logo  = outerColor.logo;
    }

    $nav.link.css('color', items);
    $nav.logo.css('fill', logo);
  },

  mount: function(section) {
    $header.addClass('active');
    _nav.color(innerColor.text, innerColor.logo);
    $nav.close.removeClass('inactive').fadeIn(t);
    $nav.menu.fadeIn(t);
    $nav.controls.fadeOut(0);
    $body.addClass('scroll-lock');

    if (typeof section !== 'undefined') {
      _nav.section(section);
    }
  },

  section: function(section) {
    setTimeout(function() { 
      $('.menu-item:not([data-menu="' + section + '"])').removeClass('active');
      $('.menu-item[data-menu="' + section + '"]').addClass('active');
      if (w >= 768) setTimeout(function() {
        $('.menu-item[data-menu="' + section + '"] .menu-item-container').fadeIn(t);
      }, 50)
    }, 500);
  },

  unmount: function() {
    $header.removeClass('active');
    if (!_nav.inFooter) {
      _nav.color(outerColor.text, outerColor.logo);
    } else {
      if (_nav.inFooter) {
        _nav.color(innerColor.text, innerColor.logo);
      } else {
        _nav.color(outerColor.text, outerColor.logo);
      }
    }
    $nav.menu.fadeOut(t);
    $nav.controls.css('display', 'inline-block');
    $nav.close.addClass('inactive').delay(t).fadeOut(0);
    setTimeout(function() {
      $nav.item.removeClass('active');
      $nav.controls.removeClass('inactive')
      $nav.dlink.removeClass('active inactive');
    }, t);

    if (!bodyLock) $body.removeClass('scroll-lock');
  },
};

var $select = {
  container : $('.pseudo-select'),
  active    : $('.select-active'),
  input     : $('.pseudo-select-input'),
  option    : $('.select-option'),
};

$select.active.click(function() {
  var select = $(this).data('select');
  $('.pseudo-select[data-select="' + select + '"]').addClass('active');
});

$select.option.click(function() {
  var option = $(this).data('option');
  var select = $(this).data('select');

  $('.select-option[data-select="' + select + '"], .pseudo-select[data-select="' + select + '"]').removeClass('active');
  $(this).addClass('active');

  $('.pseudo-select-input[data-select="' + select + '"]').val(option);
  $('.select-active-label[data-select="' + select + '"]').text(option);
});

$select.container.hover(function() {
  $(this).addClass('active');
}, function() {
  $(this).removeClass('active');
});

var $newsletter = {
  mc : {
    input   : $('#mce-EMAIL'),
    submit  : $('#mc-embedded-subscribe'),
    form    : $('#mc-embedded-subscribe-form'),
  },
  footer: {
    input       : $('#footer-newsletter-input'),
    form        : $('#footer-newsletter-form'),
    error       : $('#footer-newsletter-form .error'),
    entry       : $('#footer-newsletter-form .form-entry'),
    successful  : $('#footer-newsletter-form .form-success'),
  },
};

var _newsletter = {
  signups: [],

  check: function(email) {
    var valid   = _global.email(email);
    var unique  = ($.inArray(email, _newsletter.signups) == -1);
    if (valid && unique) {
      return true;
    } else {
      if (!valid) {
        $newsletter.footer.error.text('Please enter a valid email. Mettez une adresse email valide.');
        return false;
      }

      if (!unique) {
        $newsletter.footer.error.text('This email has already been entered. Cette adresse e-mail est déjà envoyée.');
        return false;
      }
    }
  },

  dispatch: function(email) {
    var action = $newsletter.mc.form.attr('action');
    $.ajax({
      type: 'post',
      url: action, 
      data: {
        EMAIL: email
      },
      dataType: 'jsonp',
    }).success(function(data) {
      console.log('success', data);
      _newsletter.successful(email);
    }).error(function(data) {
      console.log('error', data);
      if (data.statusText == 'success') {
        _newsletter.successful(email);
      }
    });

  },

  submit: function(email) {
    email = email || $newsletter.footer.input.val();
    if (_newsletter.check(email)) {
      $newsletter.footer.error.removeClass('active');
      $newsletter.footer.form.removeClass('error-invalid');

      _newsletter.dispatch(email);
    } else {
      $newsletter.footer.error.addClass('active');
      $newsletter.footer.form.addClass('error-invalid');
    }
  },

  successful: function(email) {
    $newsletter.footer.form.addClass('success');
    $newsletter.footer.entry.fadeOut(t);
    $newsletter.footer.successful.delay(t + 50).fadeIn(t);
    _newsletter.signups.push(email);

    setTimeout(function() {
      $newsletter.footer.input.val('').removeAttr('value');
      $newsletter.footer.form.removeClass('success');
      $newsletter.footer.successful.fadeOut(t);
      $newsletter.footer.entry.delay(t + 50).fadeIn(t);
    }, 8000);
  }
};

$newsletter.footer.form.on('submit', function(e) {
  e.preventDefault();
  _newsletter.submit();
});

$nav.label.click(function() {
  $(this).parent().toggleClass('active');
});

$nav.mlink.click(function() {
  _nav.mount();
});

$nav.close.click(function() {
  _nav.unmount();
});

$nav.dclose.click(function() {
  _nav.unmount();
});

$nav.dlink.click(function() {
  if ($(this).data('menu') == 'brands' || $(this).data('menu') == 'shop') {
    if ($(this).hasClass('active')) {
      _nav.unmount();
    } else {
      if (w >= 768) $('.menu-item-container').fadeOut(500);
      $nav.dlink.removeClass('active').addClass('inactive');
      $nav.controls.addClass('inactive');
      $(this).addClass('active');
      _nav.mount($(this).data('menu'));
    }
  }
});

$nav.brand.on('mouseenter', function() {
  if (w > 1024) {
    $nav.image.addClass('active');
    $nav.brand.addClass('inactive').removeClass('active');
    $(this).addClass('active').removeClass('inactive');
    var color = ($(this).data('color')) ? $(this).data('color') : 'rgb(' + _global.rand(0, 200) + ',' + _global.rand(0, 200) + ',' + _global.rand(0, 200) + ')'; 
    var image = ($(this).data('image')) ? $(this).data('image') : $nav.menu.data('image');

    $nav.image.append('<img class="queued-image" src="' + image + '" />');
    $('.brand-image .current-image').removeClass('current-image').addClass('destroy-image');
    
    $nav.menu.css('background', color);
    setTimeout(function() {
      $('.brand-image .queued-image').removeClass('queued-image').addClass('current-image');
    }, 0);

    setTimeout(function() {
      $('.brand-image .destroy-image').remove();
    }, t);
  }
});

$nav.brands.on('mouseleave', function() {
  $nav.menu.css('background', innerColor.bg);
  if (w > 1024) {
    $nav.brand.removeClass('active inactive');
    $nav.image.removeClass('active');
  }
});

$(document).ready(function() {
  _global.res();
  _global.elem();
  _nav.check();
});

$win.resize(function() {
  _global.res();
});

$win.on('load', function() {
  _global.init();
  _global.lazy();
});

$win.on('scroll', function() {
  _nav.check();
});

$(window).bind('pageshow', function(event) {
  if (event.originalEvent.persisted) {
    window.location.reload() 
  }
});


var redirectLocation = null;
$(document).on('click', 'a', function(e) {
  if ($(this).attr('href') && $(this).attr('target') !== '_blank') {
    e.preventDefault();
    redirectLocation = $(this).attr('href');
    $header.addClass('off');
    $body.addClass('switch initializing');

    setTimeout(function() {
      window.location.href = redirectLocation;
    }, 1000);
  }
});

var $account = {
  recover: {
    link    : $('#RecoverPassword'),
    form    : $('#RecoverPasswordForm'),
    success : $('.recover-success'),
  },
};

var _account = {
  init: function() {
    if (window.location.hash == '#recover') _account.recover();
    if (window.location.hash == '#recovered') $account.recover.success.fadeIn(t);
  },

  recover: function() {
    $account.recover.form.fadeIn(t);
    $('html, body').animate({ scrollTop: $account.recover.link.offset().top }, 1000);
  }
};

$account.recover.link.click(function() {
  _account.recover(); 
});

$(document).ready(function() {
  if (csm.template.indexOf('customers') !== -1) _account.init();
});
var $cart = {
  count : $('.item-count'),
  remove: $('.item-remove'),
  update: $('#update-cart'),
};

var _cart = {
  remove: function(variant) {
    $('#update_' + variant).val(0);
    $cart.update.trigger('click');
  },

  update: function() {
    $.getJSON('/cart.js', function(data) {
      if (data.item_count) $cart.count.addClass('active').text('(' + data.item_count + ')')
    });
  },
};

$cart.remove.click(function() {
  _cart.remove($(this).data('variant'));
});

$(document).ready(function() {
  _cart.update();
});
var $collection = {
  container   : $('.collection-container'),
  description : $('.collection-description'),
  scroll      : $('.collection-desktop-scroll'),
  filter      : $('.collection-filter button'),
  grid        : $('.product-grid'),
  products    : $('.grid-product'),
  images      : $('.grid-product-images'),
  clear       : $('.clear-filters'),
  hero        : $('.collection-hero'),
  clearH      : $('.collection-filter .clear-filters'),
  clearO      : $('.filter-overlay .clear-filters'),
  options     : $('.filter-options'),
  index       : $('.filter-carousel-index-point'),
};

$collection.scroll.click(function() {
  $('html, body').animate({ scrollTop: h }, 1000);
});

var _collection = {
  activate: function(type) {
    $('.filter-type').removeClass('active');
    $('.filter-type[data-type="' + type + '"]').addClass('active');
    $('.filter-type-options').removeClass('active');
    $('.filter-type-options[data-type="' + type + '"]').addClass('active');
  },

  init: function() {
    _collection.nav();
    _collection.size();
    if ($collection.description.find('p').length > 1) {
      $collection.description.addClass('has-more');
      $collection.description.find('p').eq(0).addClass('first-description');
    } else if ($collection.description.find('p').length == 0) {
      var content = '<p>' + $collection.description.html() + '</p>';
      $collection.description.html(content);
    }
    $filter.logo.width($('.header-logo').width());
  },

  less: function() {
    $('.collection-description p:not(:first-of-type)').fadeOut(t);
  },

  more: function() {
    $('.collection-description p:not(:first-of-type)').fadeIn(t);
  },

  nav: function() {
    if (!_nav.inFooter &&
        typeof collectionTextColor !== 'undefined') {

      _nav.color(outerColor.text, outerColor.logo);
    } else {
      _nav.color();
    }
  },

  size: function() {
    var tallest = 0;
    $collection.images.each(function() {
      var iH = $(this).find('.grid-product-image-container').eq(0).height();
      if (iH > tallest) {
        $collection.images.height(iH);
        tallest = iH;
      }
    });
  }
};

var $filter = {
  overlay : $('.filter'),
  logo    : $('.filter-logo'),
  close   : $('.filter-close'),
  types   : $('.filter-types'),
  options : $('.filter-options'),
  apply   : $('.filter-apply'),
};

var _selection = {
  brands: [],
  colors: [],
  sizes: [],
  types: [],
};

var _options = {
  brands: {},
  colors: {},
  sizes: {},
  types: {},
};

var _filters = {
  types: [],
  sizes: [],
  colors: [],
  brands: [],
};

var _filter = {
  calibrate:  0,
  queries:    [],
  subqueries: [],

  prefix: ($collection.container.data('collection')) ? 'collection_id=' + $collection.container.data('collection') + '&' : '',

  apply: function() {

    var apply = {
      types:  [],
      sizes:  [],
      colors: [],
      brands: [],
    };

    if ($('.filter-option.active').length) {
      $('.filter-option.active').each(function() {
        var option  = (typeof $(this).data('option') == 'string') ? $(this).data('option').toLowerCase() : $(this).data('option');
        //option      = ($(this).data('type') == 'sizes' || $(this).data('type') == 'colors') ? option : option.replace(/ /g, '%20');
        apply[$(this).data('type')].push(option);
      });
    } else {
      location.reload();
    }

    _filter.buttons(apply);
    _filter.fetch(apply);

    _collection.size();
    $('html, body').animate({ scrollTop: $collection.hero.innerHeight() }, 1000);
    _collection.size();
  },

  buttons: function(apply) {
    for (var key in apply) {
      var count = (apply[key].length) ? '(' + apply[key].length + ')' : '';
      $('.filter-button[data-type="' + key + '"] .filter-count').html(count);
    }
  },

  checkOptions: function() {
    var available   = {};
    var unavailable = {};
    for (key in _selection) {
      _selection[key].forEach(function(val) {
        $('.filter-option').each(function() {
          var type    = $(this).data('type');
          var option  = $(this).data('option');
          var newKey  = type + '|' + option;
          if ($.inArray(option, _options[key][val]) == -1 &&
              option  !== val &&
              type    !== key) {
            $(this).addClass('unavailable');
            unavailable[newKey] = $(this);
          } else if (type !== key) {
            $(this).addClass('available');
            available[newKey] = $(this);
          }
        })
      });
    }

    $('.filter-option.available, .filter-option.active').removeClass('inactive available unavailable');
    $('.filter-option.unavailable').addClass('inactive').removeClass('unavailable');
  },

  collect: function() {
    $collection.products.each(function() {
      var product = {
        brands  : ($(this).data('brands'))  ? $(this).data('brands').split('|') : [],
        colors  : ($(this).data('colors'))  ? $(this).data('colors').split('|') : [],
        sizes   : ($(this).data('sizes'))   ? $(this).data('sizes').split('|') : [],
        types   : ($(this).data('types'))   ? $(this).data('types').split('|') : [],
      };

      for (var key in product) {
        var tmpKey = [];
        product[key].forEach(function(value) {
          var val     = (typeof value == 'string' && key !== 'brands') ? value.trim().toLowerCase() : value.trim();
          var option  = key + '|' + val;

          if (val) {
            if  ((key !== 'colors' || $.inArray(val, tmpKey) >= 0) &&
                $.inArray(val, _filters[key]) == -1 &&
                (key !== 'colors' || val.length <= 14)) _filters[key].push(val);

            var newOption = [];
            for (var k in product) {
              if (k !== key) {
                product[k].forEach(function(cell) {
                  cell = (typeof cell == 'string' && key !== 'brands') ? cell.trim().toLowerCase() : cell.trim();
                  if (cell && $.inArray(cell, newOption) == -1) newOption.push(cell);
                });
              }
            }

            _options[key][val] = (val in _options[key]) ? _options[key][val].concat(newOption) : newOption;
          }

          tmpKey.push(val);
        });
      }
    });

    for (var key in _filters) if (key !== 'sizes') _filters[key].sort();
  },

  clean: function() {
    $('.filter-type-options').each(function() {
      if ($(this).find('.filter-option').length <= 1) {
        var type = $(this).data('type');
        $('.filter-type[data-type="' + type + '"]').remove();
        $('.filter-type-options[data-type="' + type + '"]').remove();
        $('.filter-button[data-type="' + type + '"]').remove();
      }

      if ($('.filter-button').length < 1) {
        $('.collection-filter, .filter').remove();
      }
    });
  },

  create: function() {
    for (var key in _filters) {
      $filter.options.append('<div class="filter-type-options" data-type="' + key + '"></div>');
      _filters.sizes.sort();
      _filters[key].forEach(function(val) {
        if (val.trim()) {
          if (key == 'sizes') {
            if (isNaN(parseInt(val))) {
              // Tops size
              if (val) {
                if (!$('.filter-subtype[data-subtype="tops"]').length) $('.filter-type-options[data-type="' + key + '"]').append('<div class="filter-subtype" data-subtype="tops"></div>');
                $('.filter-subtype[data-subtype="tops"]').append('<button class="tag filter-option" data-type="' + key + '" data-option="' + val + '">' + val + ' +</button>');
              }
            } else if (parseInt(val) > 26) {
              // Pants size
              if (!$('.filter-subtype[data-subtype="pants"]').length) $('.filter-type-options[data-type="' + key + '"]').append('<div class="filter-subtype" data-subtype="pants"></div>');
              $('.filter-subtype[data-subtype="pants"]').append('<button class="tag filter-option" data-type="' + key + '" data-option="' + val + '">' + val + '</button>');
            } else {
              // Shoes size
              if (!$('.filter-subtype[data-subtype="shoes"]').length) $('.filter-type-options[data-type="' + key + '"]').append('<div class="filter-subtype" data-subtype="shoes"></div>');
              $('.filter-subtype[data-subtype="shoes"]').append('<button class="tag filter-option" data-type="' + key + '" data-option="' + val + '">' + val + '</button>');
            }
          } else {
            $('.filter-type-options[data-type="' + key + '"]').append('<button class="tag filter-option" data-type="' + key + '" data-option="' + val + '">' + val + '</button>');
          }
        }
      });
    }

    var tops  = $('.filter-subtype[data-subtype="tops"]');
    var pants = $('.filter-subtype[data-subtype="pants"]');
    var shoes = $('.filter-subtype[data-subtype="shoes"]');
    $('.filter-type-options[data-type="sizes"]').empty().append(tops).append(pants).append(shoes);
  },

  fetch: function(apply) {
    _filter.queries     = [];
    _filter.subqueries  = [];
    for (var key in apply) {
      if (apply[key].length) {
        var q = _filter.getPrefix(key);

        if (key == 'colors' || key == 'sizes') {
          apply[key].forEach(function(val) {
            var query = q + '=' + val;
            if ($.inArray(query, _filter.subqueries) == -1) {
              _filter.subqueries.push(query);
            }
          });
        } else {
          apply[key].forEach(function(val) {
            var query = q + '=' + val;
            if ($.inArray(query, _filter.queries) == -1) {
              _filter.queries.push(query);
            }
          });
        }
      }
    }

    _filter.getProducts();
  },

  filterProducts: function() {
    if (_filter.queries.length) {
      var filtered = [];

      for (var i = 0; i < _filter.products.length; i++) {
        var product = _filter.products[i];

        for (var j = 0; j < _filter.queries.length; j++) {
          var request = _filter.queries[j].split('=');
          var base    = request[0];
          var target  = request[1];

          if (product[base].toLowerCase() == target) {
            filtered[product.id] = product;
          }
        }
      }
    
      _filter.products = filtered;
    }

 
    if (_filter.subqueries.length) {
      var filtered = [];

      for (var key in _filter.products) {
        var product = _filter.products[key];

        for (var j = 0; j < _filter.subqueries.length; j++) {
          var request = _filter.subqueries[j].split('=');
          var base    = request[0];
          var target  = request[1];

          for (var k = 0; k < product.options.length; k++) {
            if (product.options[k]['name'].toLowerCase() == base) {
              for (var l = 0; l < product.options[k]['values'].length; l++) {
                if (product.options[k]['values'][l].toLowerCase() == target) {
                  filtered[product.id] = product;
                }
              }
            }
          }
        }
      }

      _filter.products = filtered;

    }


    // populate grid
    _filter.grid(_filter.products);
  },

  getPrefix: function(key, val) {
    var q;
    switch (key) {
      case 'types':
        q = 'product_type';
        break;
      case 'brands':
        q = 'vendor';
        break;
      case 'colors':
        q = 'color';
        break;
      case 'sizes':
        q = 'size';
        break;
    }

    if (typeof val !== 'undefined' && val) q += '=' + val;
    return q;
  },

  getProducts: function() {
    var ajaxQuery = 'https://10a5541e912d2d3caa4566a4e0f556db:31f0ee45110f163f09cbfdb126bcb6e5@mercantile-mtl.myshopify.com/products.json?limit=250';

    $.ajax({
      type: 'GET',
      url: ajaxQuery,
      crossDomain: true,
      dataType: 'json',
      success: function(data) {
        _filter.products = data.products;

        // activate clear filter buttons
        $collection.clearH.addClass('active');
        setTimeout(function() {
          $collection.clearO.addClass('active');
        }, t*2);

        _filter.filterProducts();

        // order and reveal grid
        setTimeout(function() {
          _filter.unmount();
          _collection.size();
        }, 100);
      },
      error: function(data) {
        console.log('error:', data);
      }  
    });

  },

  getVariants: function(item) {
    var variants = {
      sizes:  [],
      colors: [],
    };

    for (var i = 0; i < item.variants.length; i++) {
      if (item.variants[i].option1) {
        var variant = item.variants[i].option1;
        variant     = (typeof variant == 'string') ? variant.toLowerCase() : variant;
        variants.sizes.push(variant);
      }
      if (item.variants[i].option2) {
        var variant = item.variants[i].option2;
        variant     = (typeof variant == 'string') ? variant.toLowerCase() : variant;
        variants.colors.push(variant);
      }
    }

    return variants;
  },

  grid: function(products) {
    $collection.grid.empty();

    if (!_global.isEmptyObject(products)) {
      for (var key in products) {
        $collection.grid.append(_filter.sanitize(products[key]));
        _collection.size();
      }
    } else {
      $collection.grid.append('<h1>0 results</h1>');
    }
    $collection.images = $('.grid-product-images');
    _collection.size();

    setTimeout(function() {
      _collection.size();
    }, t); 
  },

  init: function() {
    _filter.collect();
    _filter.create();
    _filter.clean();
  },

  mount: function() {
    $header.addClass('filter-active');
    $body.addClass('scroll-lock');
    _nav.color(innerColor.text, innerColor.logo);
    setTimeout(function() {
      $filter.overlay.fadeIn(t);
      $nav.menus.fadeOut(t);

      if (!_filter.calibrate) {
        _filter.size();
      }
    }, 700);
  },

  sanitize: function(item) {
    var image     = '';
    var variants  = _filter.getVariants(item);

    for (var i = 0; i < 2; i++) {
      if (typeof item.images[i] !== 'undefined') {
        image += '<div class="grid-product-image-container"><img class="grid-product-image" src="' + item.images[i].src + '" /></div>';
      }
    }

    var images  = '<div class="grid-product-images">' + image + '</div>';
    var vendor  = '<span class="grid-product-collection">' + item.vendor + '</span>';
    var title   = '<span class="grid-product-title">' + item.title + '</span>';
    var compare = (item.variants[0].compare_at_price) ? '<span class="grid-product-compare">' + item.variants[0].compare_at_price + '</span>' : '';
    var price   = '<span class="grid-product-price">' + compare + '$' + item.variants[0].price;
    var inner   = images + vendor + title + price;

    var product = '<div class="grid-product"><a href="/products/' + item.handle + '">' + inner + '</a></div>';
    return product;
  },

  size: function() {
    if (w > 768) {
      if ($('.slick-active').length) $collection.options.slick('unslick');
    } else {
      $('.filter-type-options').each(function() {
        $('.filter-carousel-index').append('<div class="filter-carousel-index-point carousel-index-point" data-carousel="filter"></div>');
      });
      $('.filter-carousel-index-point').eq(0).addClass('active');

      $collection.options.slick({
        adaptiveHeight: true,
        arrows : false,
      });
    }

    _filter.calibrate = 1;
  },

  unmount: function() {
    $filter.overlay.fadeOut(t);
    $nav.menus.fadeIn(t);
    $body.removeClass('scroll-lock');
    _collection.size();
    setTimeout(function() {
      $header.removeClass('filter-active');
      _nav.color(outerColor.text, outerColor.logo);
    }, 700);
  },

};

$collection.description.click(function() {
  if ($(this).hasClass('more')) {
    _collection.less();
  } else {
    _collection.more();
  }
  $(this).toggleClass('more');
});

$collection.filter.click(function() {
  _filter.mount();
});

$filter.close.click(function() {
  _filter.unmount();
});

$(document).on('click', '.filter-option', function() {
  if (!$(this).hasClass('inactive')) {
    var $elem   = $(this);
    var type    = $elem.data('type');
    var option  = $elem.data('option');
    $elem.toggleClass('active');
    if ($elem.hasClass('active')) {
      _selection[type].push(option);
    } else {
      var index = _selection[type].indexOf(option);
      _selection[type].splice(index, 1);
    }
    _filter.checkOptions();
  }
});

$(document).on('click', '.filter-type, .filter-button, .mobile-filter', function() {
  if ($(this).data('type')) { 
    _collection.activate($(this).data('type'));

    if (w < 768) {
      $collection.options.slick('slickGoTo', $('.filter-type[data-type="' + $(this).data('type') + '"]').index('.filter-type'));
    }
  } else {
    _collection.activate($('.filter-type').eq(0).data('type'));
  }
});

$filter.apply.click(function() {
  _filter.apply();
});

$collection.clear.click(function() {
  location.reload();
});

$(document).on('click', '.filter-carousel-index-point', function() {
  var carousel  = $(this).data('carousel');
  var index     = $(this).index('.carousel-index[data-carousel="' + carousel + '"] .carousel-index-point');
  
  $collection.options.slick('slickGoTo', index);
});

$collection.options.on('beforeChange', function(event, slick, current, next) {
  $('.filter-carousel-index-point').removeClass('active').eq(next).addClass('active');
  _collection.activate($('.filter-type').eq(next).data('type'));
});


$(document).ready(function() {
  if (typeof collection !== 'undefined' && collection) {
    _collection.init();
    _filter.init();
  }
});

$(window).on('resize', function() {
  if (typeof collection !== 'undefined' && collection) {
    _collection.size();
  }
});

$(window).on('load', function() {
  if (typeof collection !== 'undefined' && collection) {
    _collection.size();
  }
});
var $slider = {
  container : $('.homepage-slider'),
  slides    : $('.slide'),
  progress  : $('.section-progress'),
  slider    : $('.section-slider'),
};

var scrollThrottle = 0,
    bodyLock       = 0,
    slideLock = false;

var _slider = {
  current: 0,
  slides: $slider.slides.length,

  nextSlide: function() {
    if (_slider.current !== $slider.slides.length - 1 && slideLock === false && !$header.hasClass('active')) {
      slideLock = true;
        
      $slider.slides.eq(_slider.current).removeClass('active').addClass('inactive');
      _slider.current++;
      $slider.slides.eq(_slider.current).addClass('active');

      outerColor = {
        text : $slider.slides.eq(_slider.current).data('text-color'),
        logo : $slider.slides.eq(_slider.current).data('logo-color') || $slider.slides.eq(_slider.current).data('text-color'),
      };

      _nav.color(outerColor.text, outerColor.logo);

      //scrollThrottle = 1;

      setTimeout(function() {
        //scrollThrottle = 0;
        slideLock = false;
      }, 1080);
      _slider.updateIndex();
    }
    
  },

  prevSlide: function() {
    if (_slider.current !== 0 && slideLock === false && !$header.hasClass('active')) {
        
        slideLock = true;
        
      $slider.slides.eq(_slider.current).removeClass('active');
      _slider.current--;
      $slider.slides.eq(_slider.current).removeClass('inactive').addClass('active');

      outerColor = {
        text : $slider.slides.eq(_slider.current).data('text-color'),
        logo : $slider.slides.eq(_slider.current).data('logo-color') || $slider.slides.eq(_slider.current).data('text-color'),
      };

      _nav.color(outerColor.text, outerColor.logo);

      //scrollThrottle = 1;

      setTimeout(function() {
        //scrollThrottle = 0;
          slideLock = false;
      }, 1080);
    _slider.updateIndex();
    }
  },

  updateIndex: function() {
    var progress = (_slider.current / (_slider.slides - 1)) * 100;
    $slider.progress.css('width', progress + '%');
  }
}

// homepage vertical swipe detection:
// -mobile gestures
/*$(document).on('swipeup', function() {
    
    if(slideLock === false){
        if ($slider.container.length) _slider.nextSlide();
    }
});
$(document).on('swipedown', function() {
  console.log('down');
    if(slideLock === false){
        if ($slider.container.length) _slider.prevSlide();
    }
});*/

var scrollReset   = 700,
    timeoutCheck  = false,
    timeoutScroll;

// event listener for scrolling
// this works even when $win.scrollTop is static
function scrollDetection() {
  $win.one('mousewheel DOMMouseScroll', function (e) {
    var evt     = e.originalEvent || e;
    var deltaY  = evt.deltaY || (-1 / 40 * evt.wheelDelta) || evt.detail;

    setTimeout(function() {
      timeoutCheck  = false;
    }, scrollReset);
      
    if (deltaY > 0) {
      if ($slider.container.length && !slideLock && !timeoutCheck) {
        timeoutCheck = true;
        _slider.nextSlide();
      }
    } else {
      if ($slider.container.length && !slideLock && !timeoutCheck) {
        timeoutCheck = true;
        _slider.prevSlide();   
      }
    }
  });

  setTimeout(function() {
    scrollDetection();
  }, scrollReset);
}
scrollDetection();

var _slides = {
  check: function() {
    if (w <= 1024) homepageOverride();
  },

  init: function() {
    $body.css('overflow', 'hidden');
    _slides.size();

    outerColor = {
      text : $slider.slides.eq(_slider.current).data('text-color'),
      logo : $slider.slides.eq(_slider.current).data('logo-color') || $slider.slides.eq(_slider.current).data('text-color'),
    };

    _nav.color(outerColor.text, outerColor.logo);

    if (w <= 1024) {
      $win.on('scroll', function() {
        _slides.size();
      });
    }
  },

  size: function() {
    var orientation = (w > h) ? 'desktop-image' : 'mobile-image';
    $slider.slider.height(h);
    $slider.slides.each(function() {
      if (w > 1024) {
        $(this).height(h);
      } else {
        $(this).height(h);
      }


      var src   = $(this).data(orientation);
      var color = $(this).data('color');
      if ($(this).hasClass('image-full-width')) {
        if (src) {
          $(this).css({
            'background': 'url(' + src + ') no-repeat center',
            'background-size': 'cover',
          });
        }
      } else {
        if (w > h || !$(this).hasClass('image-slide')) {
          $(this).css('background', color);

          if (src) $(this).find('.slide-image').attr('src', src);
        } else {
          if (src) {
            $(this).css({
              'background': 'url(' + src + ') no-repeat center',
              'background-size': 'cover',
            });
          }
          $(this).find('.slide-image').addClass('inactive');
        }
      }
    });

    var csrc = $('.contact-image').data(orientation);
    $('.contact-image').height(h - $('.contact-info').outerHeight()).css({
      'background': 'url(' + csrc + ') no-repeat center',
      'background-size': 'cover',
    });;
  },
};

// binding arrow keys to slide navigation
$(document).keydown(function(e) {
  switch(e.which) {
    case 37: // left
    case 38: // up
      _slider.prevSlide();
    break;

    case 39: // right
    case 40: // down
      _slider.nextSlide();
    break;

    default: return; // exit for other keys
  }
  e.preventDefault();
});

$(document).ready(function() {
  if ($slider.container.length && csm.template == 'index') {
    $body.addClass('scroll-lock');
    $header.addClass('homepage');
    _slides.init();
    $slider.slides.eq(_slider.current).addClass('active');

    $('body').on('touchmove',function(e){
      if (!firstSwipe) _slider.nextSlide();
    }); 
  } else {
    $win.off('DOMMouseScroll mousewheel');
  }
  if ($slider.container.length) _slides.check();
});

$win.resize(function() {
  if ($slider.container.length) _slides.size();
});

$win.on('load', function() {
  var checkInt = setInterval(function() {
    if (!$body.hasClass('initializing')) {
      _slides.size();
      $('.homepage-slider').css({
        'display' : 'block',
        'opacity' : 0
      }).delay(t).animate({
        'opacity' : 1
      }, 1000);
      clearInterval(checkInt);
    }
  }, 100);
});

function homepageOverride() {
  $(document).bind('touchmove', function(e) {
    if (!$header.hasClass('active')) {
      e.preventDefault();
    }
  });
  document.addEventListener('touchstart', handleTouchStart, false);        
  document.addEventListener('touchmove', handleTouchMove, false);

  var xDown = null;                                                        
  var yDown = null;                                                        

  function handleTouchStart(evt) {     
    xDown = evt.touches[0].clientX;                                      
    yDown = evt.touches[0].clientY;                                      
  }                                      

  function handleTouchMove(evt) {
    if (!xDown || !yDown) {
      return;
    }
    var xUp = evt.touches[0].clientX;                                    
    var yUp = evt.touches[0].clientY;

    var xDiff = xDown - xUp;
    var yDiff = yDown - yUp;

    if (Math.abs(xDiff) > Math.abs(yDiff)) {
      if (xDiff > 0) {
        /* left swipe */ 
      } else {
        /* right swipe */
      }                       
    } else {
      if (yDiff > 0) {
        if (!slideLock) _slider.nextSlide();
      } else { 
        if (!slideLock) _slider.prevSlide();
      }                                                                 
    }
    
    /* reset values */
    xDown = null;
    yDown = null;                                             
  }
}
var $story = {
  tagline : $('.our-story-section .tagline'),
  brands  : $('.our-story-brands'),
  brand   : $('.our-story-brand'),
  press   : $('.press-link'),
  pressC  : $('.press-container'),
  index   : {
    points: $('.our-story-brands-section .carousel-index-point'),
  }
};

var _story = {
  press: $story.pressC.css('background-color'),
  init: function() {
    if (w <= 1024) {
      $story.brands.slick({
        arrows: false,
      });
    } else {
      setInterval(function() {
        _story.tagline();
      }, 15);
    }
  },

  tagPos: 1000,
  tagline: function() {
    if (parseInt($story.tagline.css('left')) < ((-1) * parseInt($story.tagline.width()) - 100)) _story.tagPos = 1000;
    $story.tagline.css('left', (_story.tagPos / 10) + '%');
    _story.tagPos = _story.tagPos - 5;
  },
};

$story.press.hover(function() {
  if (w > 1024) {
    var press = $(this).data('press');
    var color = $(this).data('color');
    $story.pressC.css('background-color', color);
    $('.press-content[data-press="' + press + '"]').addClass('active');
  }
}, function() {
  if (w > 1024) {
    var press = $(this).data('press');
    $story.pressC.css('background-color', _story.press);
    $('.press-content[data-press="' + press + '"]').removeClass('active');
  }
});

$story.index.points.click(function() {
  var carousel  = $(this).data('carousel');
  var index     = $(this).index('.carousel-index[data-carousel="' + carousel + '"] .carousel-index-point');
  
  $story[carousel].slick('slickGoTo', index);
});

$story.brands.on('beforeChange', function(event, slick, current, next) {
  $story.index.points.removeClass('active').eq(next).addClass('active');
});

$win.on('scroll', function() {
  if (csm.template == 'page.our-story') {
    $story.brand.each(function() {
      if ($(this).offset().top < $(document).scrollTop() + h) $(this).removeClass('inactive');
    });
  }
});

$(document).ready(function() {
  if (csm.template == 'page.our-story') {
    _story.init();
    _nav.color('black', 'black');

    $('.full-height').each(function() {
      if (!($(this).hasClass('brand-container') && w < 1024)) $(this).height(h - parseInt($(this).css('padding-top')) - parseInt($(this).css('padding-bottom')));
    });

    outerColor.text = 'black';
    outerColor.logo = 'black';
  }
});
var $product = {
  breadcrumbs : $('.product-breadcrumbs'),
  container   : $('.product-container'),
  images      : $('.product-images'),
  image       : $('.product-image'),
  related     : $('.related-products'),
  add         : $('.product-add'),
  info        : $('.product-information'),
  compare     : $('.product-compared-price'),
  price       : $('.product-price'),
  variant     : $('.variant-option'),
  low         : $('.low-stock'),
  index       : {
    points  : $('.product-container .carousel-index-point'),
    images  : $('.product-container .carousel-index-point[data-carousel="images"]'),
    related : $('.product-container .carousel-index-point[data-carousel="related"]')
  },
};

var _product = {
  mobile: (csm.template == 'product') ? $(document).height() - $footer.outerHeight() - $product.add.outerHeight() - $(window).height() : null,
  desktop: (csm.template == 'product') ? $product.image.last().offset().top + $product.image.last().height() - $product.info.outerHeight() - $product.breadcrumbs.outerHeight() - parseInt($product.container.css('padding-top')) + 60 : null,

  init: function() {
    _collection.size();
    if (w <= 1024) {
      $product.images.slick({
        arrows: false,
      });
      $product.related.slick({
        arrows: false,
      });
    }
  },
};

var $a2c = {
  button  : $('.add-to-cart'),
};

var _a2c = {
  cart: null,

  add: function(variant) {
    $.ajax({
      type: "POST",
      url: '/cart/add.js',
      data: {
        quantity: 1,
        id: variant,
      },
      success: function(data) {
        _cart.update();
        $a2c.button.removeClass('add').addClass('added').prop('disabled', true);
        setTimeout(function() {
          $a2c.button.removeClass('added').addClass('add').prop('disabled', false);
          _a2c.adjust(variant);
        }, 2000);
      },
      dataType: 'json'
    }).error(function(data) {
      console.log('err', data);
      $a2c.button.removeClass('add').addClass('error');
      setTimeout(function() {
        $a2c.button.addClass('add').removeClass('error');
        _a2c.update();
      }, 8000);
    });
  },

  adjust: function(variant, quantity) {
    if (typeof quantity == 'undefined') quantity = 1;

    var inventory = parseInt($('.variant-option[data-variant="' + variant + '"]').data('inventory'));
    inventory -= quantity;

    $('.variant-option[data-variant="' + variant + '"]').data('inventory', inventory).attr('data-inventory', inventory);
    $a2c.button.data('inventory', inventory).attr('data-inventory', inventory);
    _a2c.update();
  },

  init: function() {
    _a2c.update();
    $.getJSON('/cart.js', function(data) {
      _a2c.cart = data;
      if (data.items) {
        for (var i = 0; i < data.items.length; i++) {
          var variant   = data.items[i].id;
          var quantity  = data.items[i].quantity;

          if ($('.variant-option[data-variant="' + variant + '"]').length) _a2c.adjust(variant, quantity);
        }
      }
      _a2c.update();
    });

  },

  update: function() {
    if ($a2c.button.data('inventory')) {
      $a2c.button.removeClass('oos added').addClass('add');

      if ($a2c.button.data('inventory') == 1) {
        $product.low.addClass('active');
      } else {
        $product.low.removeClass('active');
      }
    } else {
      $a2c.button.removeClass('add added').addClass('oos');
      $product.low.removeClass('active');
    }
  },
};

$('.product-text-option').click(function() {
  $('.product-text-option, .product-text-container').removeClass('active');
  $(this).addClass('active');

  var option = $(this).data('option');
  $('.product-text-container[data-option="' + option + '"]').addClass('active');
});

$product.index.points.click(function() {
  var carousel  = $(this).data('carousel');
  var index     = $(this).index('.carousel-index[data-carousel="' + carousel + '"] .carousel-index-point');
  
  $product[carousel].slick('slickGoTo', index);
});

$product.images.on('beforeChange', function(event, slick, current, next) {
  $product.index.images.removeClass('active').eq(next).addClass('active');
});

$product.related.on('beforeChange', function(event, slick, current, next) {
  $product.index.related.removeClass('active').eq(next).addClass('active');
});

$product.variant.click(function() {
  $a2c.button.data({
    'inventory' : $(this).data('inventory'),
    'variant'   : $(this).data('variant'),
  }).attr({
    'data-inventory' : $(this).data('inventory'),
    'data-variant'   : $(this).data('variant'),
  });
  _a2c.update();

  $product.price.html('$' + (Math.floor(parseInt($(this).data('price')) / 100)));
  $('.mobile-price').html('$' + (Math.floor(parseInt($(this).data('price')) / 100)));
  if ($(this).data('compare') && parseInt($(this).data('compare')) > parseInt($(this).data('price'))) {
    $product.compare.html('$' + (Math.floor(parseInt($(this).data('compare')) / 100))).addClass('active');
    $('.mobile-compare').html('$' + (Math.floor(parseInt($(this).data('compare')) / 100))).addClass('active');
  } else {
    $product.compare.removeClass('active');
    $('.mobile-compare').removeClass('active');
  }
  
});

$a2c.button.click(function() {
  if (parseInt($(this).data('inventory')) &&
      !$(this).hasClass('oos') &&
      !$(this).hasClass('error')) {
    _a2c.add($(this).data('variant'));
  }
});

$(document).ready(function() {
  if (csm.template == 'product') {
    _product.init();
    _a2c.init();
  }
});

$(window).scroll(function() {
  if (csm.template == 'product') {
    if (w > 1024) {
      $product.add.fadeIn(t);

      if ($(document).scrollTop() > 80) {
        $product.info.addClass('fixed-top');

        var imageTop = $product.image.last().offset().top + $product.image.last().height() - $product.info.outerHeight();
        if ($(document).scrollTop() >= imageTop - 60) {
          $product.info.removeClass('fixed-top').css('top', imageTop);
        } else {
          $product.info.addClass('fixed-top').css('top', '60px');
        }
      } else {
        $product.info.removeClass('fixed-top').removeAttr('style');
        $('.product-header').removeClass('header-active');
      }
    } else {
      if ($(document).scrollTop() > _product.mobile) {
        $product.add.fadeOut(t);
      } else {
        $product.add.fadeIn(t);
      }
    }
  }
});

$(window).on('resize', function() {
  _product.mobile   = $(document).height() - $footer.outerHeight() - $product.add.outerHeight() - $(window).height();
});

$(window).on('load', function() {
  _product.mobile   = $(document).height() - $footer.outerHeight() - $product.add.outerHeight() - $(window).height();
});



// SEARCH


window.theme = window.theme || {};

theme.Sections = function Sections() {
  this.constructors = {};
  this.instances = [];

  document.addEventListener(
    'shopify:section:load',
    this._onSectionLoad.bind(this)
  );
  document.addEventListener(
    'shopify:section:unload',
    this._onSectionUnload.bind(this)
  );
  document.addEventListener(
    'shopify:section:select',
    this._onSelect.bind(this)
  );
  document.addEventListener(
    'shopify:section:deselect',
    this._onDeselect.bind(this)
  );
  document.addEventListener(
    'shopify:block:select',
    this._onBlockSelect.bind(this)
  );
  document.addEventListener(
    'shopify:block:deselect',
    this._onBlockDeselect.bind(this)
  );
};


window.slate = window.slate || {};

/**
 * Slate utilities
 * -----------------------------------------------------------------------------
 * A collection of useful utilities to help build your theme
 *
 *
 * @namespace utils
 */

slate.utils = {
  /**
   * Get the query params in a Url
   * Ex
   * https://mysite.com/search?q=noodles&b
   * getParameterByName('q') = "noodles"
   * getParameterByName('b') = "" (empty value)
   * getParameterByName('test') = null (absent)
   */
  getParameterByName: function(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[[\]]/g, '\\$&');
    var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
      results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, ' '));
  },

  resizeSelects: function(selects) {
    selects.forEach(function(select) {
      var arrowWidth = 55;

      var test = document.createElement('span');
      test.innerHTML = select.selectedOptions[0].label;

      document.querySelector('.site-footer').appendChild(test);

      var width = test.offsetWidth + arrowWidth;
      test.remove();

      select.style.width = width + 'px';
    });
  },

  keyboardKeys: {
    TAB: 9,
    ENTER: 13,
    ESCAPE: 27,
    LEFTARROW: 37,
    RIGHTARROW: 39
  }
};

window.slate = window.slate || {};

/**
 * iFrames
 * -----------------------------------------------------------------------------
 * Wrap videos in div to force responsive layout.
 *
 * @namespace iframes
 */

slate.rte = {
  /**
   * Wrap tables in a container div to make them scrollable when needed
   *
   * @param {object} options - Options to be used
   * @param {NodeList} options.tables - Elements of the table(s) to wrap
   * @param {string} options.tableWrapperClass - table wrapper class name
   */
  wrapTable: function(options) {
    options.tables.forEach(function(table) {
      var wrapper = document.createElement('div');
      wrapper.classList.add(options.tableWrapperClass);

      table.parentNode.insertBefore(wrapper, table);
      wrapper.appendChild(table);
    });
  },

  /**
   * Wrap iframes in a container div to make them responsive
   *
   * @param {object} options - Options to be used
   * @param {NodeList} options.iframes - Elements of the iframe(s) to wrap
   * @param {string} options.iframeWrapperClass - class name used on the wrapping div
   */
  wrapIframe: function(options) {
    options.iframes.forEach(function(iframe) {
      var wrapper = document.createElement('div');
      wrapper.classList.add(options.iframeWrapperClass);

      iframe.parentNode.insertBefore(wrapper, iframe);
      wrapper.appendChild(iframe);

      iframe.src = iframe.src;
    });
  }
};

window.slate = window.slate || {};

/**
 * A11y Helpers
 * -----------------------------------------------------------------------------
 * A collection of useful functions that help make your theme more accessible
 * to users with visual impairments.
 *
 *
 * @namespace a11y
 */

slate.a11y = {
  state: {
    firstFocusable: null,
    lastFocusable: null
  },
  /**
   * For use when focus shifts to a container rather than a link
   * eg for In-page links, after scroll, focus shifts to content area so that
   * next `tab` is where user expects
   *
   * @param {HTMLElement} element - The element to be acted upon
   */
  pageLinkFocus: function(element) {
    if (!element) return;
    var focusClass = 'js-focus-hidden';

    element.setAttribute('tabIndex', '-1');
    element.focus();
    element.classList.add(focusClass);
    element.addEventListener('blur', callback, { once: true });

    function callback() {
      element.classList.remove(focusClass);
      element.removeAttribute('tabindex');
    }
  },

  /**
   * Traps the focus in a particular container
   *
   * @param {object} options - Options to be used
   * @param {HTMLElement} options.container - Container to trap focus within
   * @param {HTMLElement} options.elementToFocus - Element to be focused when focus leaves container
   */
  trapFocus: function(options) {
    var focusableElements = Array.from(
      options.container.querySelectorAll(
        'button, [href], input, select, textarea, [tabindex]:not([tabindex^="-"])'
      )
    ).filter(function(element) {
      var width = element.offsetWidth;
      var height = element.offsetHeight;

      return (
        width !== 0 &&
        height !== 0 &&
        getComputedStyle(element).getPropertyValue('display') !== 'none'
      );
    });

    this.state.firstFocusable = focusableElements[0];
    this.state.lastFocusable = focusableElements[focusableElements.length - 1];

    if (!options.elementToFocus) {
      options.elementToFocus = options.container;
    }

    options.container.setAttribute('tabindex', '-1');
    options.elementToFocus.focus();

    this._setupHandlers();

    document.addEventListener('focusin', this._onFocusInHandler);
    document.addEventListener('focusout', this._onFocusOutHandler);
  },

  _setupHandlers: function() {
    if (!this._onFocusInHandler) {
      this._onFocusInHandler = this._onFocusIn.bind(this);
    }

    if (!this._onFocusOutHandler) {
      this._onFocusOutHandler = this._onFocusIn.bind(this);
    }

    if (!this._manageFocusHandler) {
      this._manageFocusHandler = this._manageFocus.bind(this);
    }
  },

  _onFocusOut: function() {
    document.removeEventListener('keydown', this._manageFocusHandler);
  },

  _onFocusIn: function(evt) {
    if (
      evt.target !== this.state.lastFocusable &&
      evt.target !== this.state.firstFocusable
    )
      return;

    document.addEventListener('keydown', this._manageFocusHandler);
  },

  _manageFocus: function(evt) {
    if (evt.keyCode !== slate.utils.keyboardKeys.TAB) return;

    /**
     * On the last focusable element and tab forward,
     * focus the first element.
     */
    if (evt.target === this.state.lastFocusable && !evt.shiftKey) {
      evt.preventDefault();
      this.state.firstFocusable.focus();
    }
    /**
     * On the first focusable element and tab backward,
     * focus the last element.
     */
    if (evt.target === this.state.firstFocusable && evt.shiftKey) {
      evt.preventDefault();
      this.state.lastFocusable.focus();
    }
  },

  /**
   * Removes the trap of focus in a particular container
   *
   * @param {object} options - Options to be used
   * @param {HTMLElement} options.container - Container to trap focus within
   */
  removeTrapFocus: function(options) {
    if (options.container) {
      options.container.removeAttribute('tabindex');
    }
    document.removeEventListener('focusin', this._onFocusInHandler);
  },

  /**
   * Add aria-describedby attribute to external and new window links
   *
   * @param {object} options - Options to be used
   * @param {object} options.messages - Custom messages to be used
   * @param {HTMLElement} options.links - Specific links to be targeted
   */
  accessibleLinks: function(options) {
    var body = document.querySelector('body');

    var idSelectors = {
      newWindow: 'a11y-new-window-message',
      external: 'a11y-external-message',
      newWindowExternal: 'a11y-new-window-external-message'
    };

    if (options.links === undefined || !options.links.length) {
      options.links = document.querySelectorAll(
        'a[href]:not([aria-describedby])'
      );
    }

    function generateHTML(customMessages) {
      if (typeof customMessages !== 'object') {
        customMessages = {};
      }

      var messages = Object.assign(
        {
          newWindow: 'Opens in a new window.',
          external: 'Opens external website.',
          newWindowExternal: 'Opens external website in a new window.'
        },
        customMessages
      );

      var container = document.createElement('ul');
      var htmlMessages = '';

      for (var message in messages) {
        htmlMessages +=
          '<li id=' + idSelectors[message] + '>' + messages[message] + '</li>';
      }

      container.setAttribute('hidden', true);
      container.innerHTML = htmlMessages;

      body.appendChild(container);
    }

    function _externalSite(link) {
      var hostname = window.location.hostname;

      return link.hostname !== hostname;
    }

    options.links.forEach(function(link) {
      var target = link.getAttribute('target');
      var rel = link.getAttribute('rel');
      var isExternal = _externalSite(link);
      var isTargetBlank = target === '_blank';

      if (isExternal) {
        link.setAttribute('aria-describedby', idSelectors.external);
      }

      if (isTargetBlank) {
        if (!rel || rel.indexOf('noopener') === -1) {
          var relValue = rel === undefined ? '' : rel + ' ';
          relValue = relValue + 'noopener';
          link.setAttribute('rel', relValue);
        }

        link.setAttribute('aria-describedby', idSelectors.newWindow);
      }

      if (isExternal && isTargetBlank) {
        link.setAttribute('aria-describedby', idSelectors.newWindowExternal);
      }
    });

    generateHTML(options.messages);
  }
};

this.Shopify = this.Shopify || {};
this.Shopify.theme = this.Shopify.theme || {};
this.Shopify.theme.PredictiveSearch = (function() {
  'use strict';

  function validateQuery(query) {
    var error;

    if (query === null || query === undefined) {
      error = new TypeError("'query' is missing");
      error.type = 'argument';
      throw error;
    }

    if (typeof query !== 'string') {
      error = new TypeError("'query' is not a string");
      error.type = 'argument';
      throw error;
    }
  }

  function GenericError() {
    var error = Error.call(this);

    error.name = 'Server error';
    error.message = 'Something went wrong on the server';
    error.status = 500;

    return error;
  }

  function NotFoundError(status) {
    var error = Error.call(this);

    error.name = 'Not found';
    error.message = 'Not found';
    error.status = status;

    return error;
  }

  function ServerError() {
    var error = Error.call(this);

    error.name = 'Server error';
    error.message = 'Something went wrong on the server';
    error.status = 500;

    return error;
  }

  function ContentTypeError(status) {
    var error = Error.call(this);

    error.name = 'Content-Type error';
    error.message = 'Content-Type was not provided or is of wrong type';
    error.status = status;

    return error;
  }

  function JsonParseError(status) {
    var error = Error.call(this);

    error.name = 'JSON parse error';
    error.message = 'JSON syntax error';
    error.status = status;

    return error;
  }

  function ThrottledError(status, name, message, retryAfter) {
    var error = Error.call(this);

    error.name = name;
    error.message = message;
    error.status = status;
    error.retryAfter = retryAfter;

    return error;
  }

  function InvalidParameterError(status, name, message) {
    var error = Error.call(this);

    error.name = name;
    error.message = message;
    error.status = status;

    return error;
  }

  function ExpectationFailedError(status, name, message) {
    var error = Error.call(this);

    error.name = name;
    error.message = message;
    error.status = status;

    return error;
  }

  function request(searchUrl, queryParams, query, onSuccess, onError) {
    var xhr = new XMLHttpRequest();
    var route = searchUrl + '/suggest.json';

    xhr.onreadystatechange = function() {
      if (xhr.readyState !== XMLHttpRequest.DONE) {
        return;
      }

      var contentType = xhr.getResponseHeader('Content-Type');

      if (xhr.status >= 500) {
        onError(new ServerError());

        return;
      }

      if (xhr.status === 404) {
        onError(new NotFoundError(xhr.status));

        return;
      }

      if (
        typeof contentType !== 'string' ||
        contentType.toLowerCase().match('application/json') === null
      ) {
        onError(new ContentTypeError(xhr.status));

        return;
      }

      if (xhr.status === 417) {
        try {
          var invalidParameterJson = JSON.parse(xhr.responseText);

          onError(
            new InvalidParameterError(
              xhr.status,
              invalidParameterJson.message,
              invalidParameterJson.description
            )
          );
        } catch (error) {
          onError(new JsonParseError(xhr.status));
        }

        return;
      }

      if (xhr.status === 422) {
        try {
          var expectationFailedJson = JSON.parse(xhr.responseText);

          onError(
            new ExpectationFailedError(
              xhr.status,
              expectationFailedJson.message,
              expectationFailedJson.description
            )
          );
        } catch (error) {
          onError(new JsonParseError(xhr.status));
        }

        return;
      }

      if (xhr.status === 429) {
        try {
          var throttledJson = JSON.parse(xhr.responseText);

          onError(
            new ThrottledError(
              xhr.status,
              throttledJson.message,
              throttledJson.description,
              xhr.getResponseHeader('Retry-After')
            )
          );
        } catch (error) {
          onError(new JsonParseError(xhr.status));
        }

        return;
      }

      if (xhr.status === 200) {
        try {
          var res = JSON.parse(xhr.responseText);
          res.query = query;
          onSuccess(res);
        } catch (error) {
          onError(new JsonParseError(xhr.status));
        }

        return;
      }

      try {
        var genericErrorJson = JSON.parse(xhr.responseText);
        onError(
          new GenericError(
            xhr.status,
            genericErrorJson.message,
            genericErrorJson.description
          )
        );
      } catch (error) {
        onError(new JsonParseError(xhr.status));
      }

      return;
    };

    xhr.open(
      'get',
      route + '?q=' + encodeURIComponent(query) + '&' + queryParams
    );

    xhr.setRequestHeader('Content-Type', 'application/json');

    xhr.send();
  }

  function Cache(config) {
    this._store = {};
    this._keys = [];
    if (config && config.bucketSize) {
      this.bucketSize = config.bucketSize;
    } else {
      this.bucketSize = 20;
    }
  }

  Cache.prototype.set = function(key, value) {
    if (this.count() >= this.bucketSize) {
      var deleteKey = this._keys.splice(0, 1);
      this.delete(deleteKey);
    }

    this._keys.push(key);
    this._store[key] = value;

    return this._store;
  };

  Cache.prototype.get = function(key) {
    return this._store[key];
  };

  Cache.prototype.has = function(key) {
    return Boolean(this._store[key]);
  };

  Cache.prototype.count = function() {
    return Object.keys(this._store).length;
  };

  Cache.prototype.delete = function(key) {
    var exists = Boolean(this._store[key]);
    delete this._store[key];
    return exists && !this._store[key];
  };

  function Dispatcher() {
    this.events = {};
  }

  Dispatcher.prototype.on = function(eventName, callback) {
    var event = this.events[eventName];
    if (!event) {
      event = new DispatcherEvent(eventName);
      this.events[eventName] = event;
    }
    event.registerCallback(callback);
  };

  Dispatcher.prototype.off = function(eventName, callback) {
    var event = this.events[eventName];
    if (event && event.callbacks.indexOf(callback) > -1) {
      event.unregisterCallback(callback);
      if (event.callbacks.length === 0) {
        delete this.events[eventName];
      }
    }
  };

  Dispatcher.prototype.dispatch = function(eventName, payload) {
    var event = this.events[eventName];
    if (event) {
      event.fire(payload);
    }
  };

  function DispatcherEvent(eventName) {
    this.eventName = eventName;
    this.callbacks = [];
  }

  DispatcherEvent.prototype.registerCallback = function(callback) {
    this.callbacks.push(callback);
  };

  DispatcherEvent.prototype.unregisterCallback = function(callback) {
    var index = this.callbacks.indexOf(callback);
    if (index > -1) {
      this.callbacks.splice(index, 1);
    }
  };

  DispatcherEvent.prototype.fire = function(payload) {
    var callbacks = this.callbacks.slice(0);
    callbacks.forEach(function(callback) {
      callback(payload);
    });
  };

  function debounce(func, wait) {
    var timeout = null;
    return function() {
      var context = this;
      var args = arguments;
      clearTimeout(timeout);
      timeout = setTimeout(function() {
        timeout = null;
        func.apply(context, args);
      }, wait || 0);
    };
  }

  function objectToQueryParams(obj, parentKey) {
    var output = '';
    parentKey = parentKey || null;

    Object.keys(obj).forEach(function(key) {
      var outputKey = key + '=';
      if (parentKey) {
        outputKey = parentKey + '[' + key + ']';
      }

      switch (trueTypeOf(obj[key])) {
        case 'object':
          output += objectToQueryParams(obj[key], parentKey ? outputKey : key);
          break;
        case 'array':
          output += outputKey + '=' + obj[key].join(',') + '&';
          break;
        default:
          if (parentKey) {
            outputKey += '=';
          }
          output += outputKey + encodeURIComponent(obj[key]) + '&';
          break;
      }
    });

    return output;
  }

  function trueTypeOf(obj) {
    return Object.prototype.toString
      .call(obj)
      .slice(8, -1)
      .toLowerCase();
  }

  var DEBOUNCE_RATE = 10;
  var requestDebounced = debounce(request, DEBOUNCE_RATE);

  function PredictiveSearch(params, searchUrl) {
    if (!params) {
      throw new TypeError('No params object was specified');
    }

    this.searchUrl = searchUrl;

    this._retryAfter = null;
    this._currentQuery = null;

    this.dispatcher = new Dispatcher();
    this.cache = new Cache({ bucketSize: 40 });

    this.queryParams = objectToQueryParams(params);
  }

  PredictiveSearch.TYPES = {
    PRODUCT: 'product',
    PAGE: 'page',
    ARTICLE: 'article'
  };

  PredictiveSearch.FIELDS = {
    AUTHOR: 'author',
    BODY: 'body',
    PRODUCT_TYPE: 'product_type',
    TAG: 'tag',
    TITLE: 'title',
    VARIANTS_BARCODE: 'variants.barcode',
    VARIANTS_SKU: 'variants.sku',
    VARIANTS_TITLE: 'variants.title',
    VENDOR: 'vendor'
  };

  PredictiveSearch.UNAVAILABLE_PRODUCTS = {
    SHOW: 'show',
    HIDE: 'hide',
    LAST: 'last'
  };

  PredictiveSearch.prototype.query = function query(query) {
    try {
      validateQuery(query);
    } catch (error) {
      this.dispatcher.dispatch('error', error);
      return;
    }

    if (query === '') {
      return this;
    }

    this._currentQuery = normalizeQuery(query);
    var cacheResult = this.cache.get(this._currentQuery);
    if (cacheResult) {
      this.dispatcher.dispatch('success', cacheResult);
      return this;
    }

    requestDebounced(
      this.searchUrl,
      this.queryParams,
      query,
      function(result) {
        this.cache.set(normalizeQuery(result.query), result);
        if (normalizeQuery(result.query) === this._currentQuery) {
          this._retryAfter = null;
          this.dispatcher.dispatch('success', result);
        }
      }.bind(this),
      function(error) {
        if (error.retryAfter) {
          this._retryAfter = error.retryAfter;
        }
        this.dispatcher.dispatch('error', error);
      }.bind(this)
    );

    return this;
  };

  PredictiveSearch.prototype.on = function on(eventName, callback) {
    this.dispatcher.on(eventName, callback);

    return this;
  };

  PredictiveSearch.prototype.off = function on(eventName, callback) {
    this.dispatcher.off(eventName, callback);

    return this;
  };

  function normalizeQuery(query) {
    if (typeof query !== 'string') {
      return null;
    }

    return query
      .trim()
      .replace(' ', '-')
      .toLowerCase();
  }

  return PredictiveSearch;
})();

this.Shopify = this.Shopify || {};
this.Shopify.theme = this.Shopify.theme || {};
this.Shopify.theme.PredictiveSearchComponent = (function(PredictiveSearch) {
  'use strict';

  PredictiveSearch =
    PredictiveSearch && PredictiveSearch.hasOwnProperty('default')
      ? PredictiveSearch['default']
      : PredictiveSearch;

  var DEFAULT_PREDICTIVE_SEARCH_API_CONFIG = {
    resources: {
      type: [PredictiveSearch.TYPES.PRODUCT],
      options: {
        unavailable_products: PredictiveSearch.UNAVAILABLE_PRODUCTS.LAST,
        fields: [
          PredictiveSearch.FIELDS.TITLE,
          PredictiveSearch.FIELDS.VENDOR,
          PredictiveSearch.FIELDS.PRODUCT_TYPE,
          PredictiveSearch.FIELDS.VARIANTS_TITLE
        ]
      }
    }
  };

  function PredictiveSearchComponent(config) {
    // validate config
    if (
      !config ||
      !config.selectors ||
      !config.selectors.input ||
      !isString(config.selectors.input) ||
      !config.selectors.result ||
      !isString(config.selectors.result) ||
      !config.resultTemplateFct ||
      !isFunction(config.resultTemplateFct) ||
      !config.numberOfResultsTemplateFct ||
      !isFunction(config.numberOfResultsTemplateFct) ||
      !config.loadingResultsMessageTemplateFct ||
      !isFunction(config.loadingResultsMessageTemplateFct)
    ) {
      var error = new TypeError(
        'PredictiveSearchComponent config is not valid'
      );
      error.type = 'argument';
      throw error;
    }

    // Find nodes
    this.nodes = findNodes(config.selectors);

    // Validate nodes
    if (!isValidNodes(this.nodes)) {
      // eslint-disable-next-line no-console
      console.warn('Could not find valid nodes');
      return;
    }

    this.searchUrl = config.searchUrl || '/search';

    // Store the keyword that was used for the search
    this._searchKeyword = '';

    // Assign result template
    this.resultTemplateFct = config.resultTemplateFct;

    // Assign number of results template
    this.numberOfResultsTemplateFct = config.numberOfResultsTemplateFct;

    // Assign loading state template function
    this.loadingResultsMessageTemplateFct =
      config.loadingResultsMessageTemplateFct;

    // Assign number of search results
    this.numberOfResults = config.numberOfResults || 4;

    // Set classes
    this.classes = {
      visibleVariant: config.visibleVariant
        ? config.visibleVariant
        : 'predictive-search-wrapper--visible',
      itemSelected: config.itemSelectedClass
        ? config.itemSelectedClass
        : 'predictive-search-item--selected',
      clearButtonVisible: config.clearButtonVisibleClass
        ? config.clearButtonVisibleClass
        : 'predictive-search__clear-button--visible'
    };

    this.selectors = {
      searchResult: config.searchResult
        ? config.searchResult
        : '[data-search-result]'
    };

    // Assign callbacks
    this.callbacks = assignCallbacks(config);

    // Add input attributes
    addInputAttributes(this.nodes.input);

    // Add input event listeners
    this._addInputEventListeners();

    // Add body listener
    this._addBodyEventListener();

    // Add accessibility announcer
    this._addAccessibilityAnnouncer();

    // Display the reset button if the input is not empty
    this._toggleClearButtonVisibility();

    // Instantiate Predictive Search API
    this.predictiveSearch = new PredictiveSearch(
      config.PredictiveSearchAPIConfig
        ? config.PredictiveSearchAPIConfig
        : DEFAULT_PREDICTIVE_SEARCH_API_CONFIG,
      this.searchUrl
    );

    // Add predictive search success event listener
    this.predictiveSearch.on(
      'success',
      this._handlePredictiveSearchSuccess.bind(this)
    );

    // Add predictive search error event listener
    this.predictiveSearch.on(
      'error',
      this._handlePredictiveSearchError.bind(this)
    );
  }

  /**
   * Private methods
   */
  function findNodes(selectors) {
    return {
      input: document.querySelector(selectors.input),
      reset: document.querySelector(selectors.reset),
      result: document.querySelector(selectors.result)
    };
  }

  function isValidNodes(nodes) {
    if (
      !nodes ||
      !nodes.input ||
      !nodes.result ||
      nodes.input.tagName !== 'INPUT'
    ) {
      return false;
    }

    return true;
  }

  function assignCallbacks(config) {
    return {
      onBodyMousedown: config.onBodyMousedown,
      onBeforeOpen: config.onBeforeOpen,
      onOpen: config.onOpen,
      onBeforeClose: config.onBeforeClose,
      onClose: config.onClose,
      onInputFocus: config.onInputFocus,
      onInputKeyup: config.onInputKeyup,
      onInputBlur: config.onInputBlur,
      onInputReset: config.onInputReset,
      onBeforeDestroy: config.onBeforeDestroy,
      onDestroy: config.onDestroy
    };
  }

  function addInputAttributes(input) {
    input.setAttribute('autocorrect', 'off');
    input.setAttribute('autocomplete', 'off');
    input.setAttribute('autocapitalize', 'off');
    input.setAttribute('spellcheck', 'false');
  }

  function removeInputAttributes(input) {
    input.removeAttribute('autocorrect', 'off');
    input.removeAttribute('autocomplete', 'off');
    input.removeAttribute('autocapitalize', 'off');
    input.removeAttribute('spellcheck', 'false');
  }

  /**
   * Public variables
   */
  PredictiveSearchComponent.prototype.isResultVisible = false;
  PredictiveSearchComponent.prototype.results = {};

  /**
   * "Private" variables
   */
  PredictiveSearchComponent.prototype._latencyTimer = null;
  PredictiveSearchComponent.prototype._resultNodeClicked = false;

  /**
   * "Private" instance methods
   */
  PredictiveSearchComponent.prototype._addInputEventListeners = function() {
    var input = this.nodes.input;
    var reset = this.nodes.reset;

    if (!input) {
      return;
    }

    this._handleInputFocus = this._handleInputFocus.bind(this);
    this._handleInputBlur = this._handleInputBlur.bind(this);
    this._handleInputKeyup = this._handleInputKeyup.bind(this);
    this._handleInputKeydown = this._handleInputKeydown.bind(this);

    input.addEventListener('focus', this._handleInputFocus);
    input.addEventListener('blur', this._handleInputBlur);
    input.addEventListener('keyup', this._handleInputKeyup);
    input.addEventListener('keydown', this._handleInputKeydown);

    if (reset) {
      this._handleInputReset = this._handleInputReset.bind(this);
      reset.addEventListener('click', this._handleInputReset);
    }
  };

  PredictiveSearchComponent.prototype._removeInputEventListeners = function() {
    var input = this.nodes.input;

    input.removeEventListener('focus', this._handleInputFocus);
    input.removeEventListener('blur', this._handleInputBlur);
    input.removeEventListener('keyup', this._handleInputKeyup);
    input.removeEventListener('keydown', this._handleInputKeydown);
  };

  PredictiveSearchComponent.prototype._addBodyEventListener = function() {
    this._handleBodyMousedown = this._handleBodyMousedown.bind(this);

    document
      .querySelector('body')
      .addEventListener('mousedown', this._handleBodyMousedown);
  };

  PredictiveSearchComponent.prototype._removeBodyEventListener = function() {
    document
      .querySelector('body')
      .removeEventListener('mousedown', this._handleBodyMousedown);
  };

  PredictiveSearchComponent.prototype._removeClearButtonEventListener = function() {
    var reset = this.nodes.reset;

    if (!reset) {
      return;
    }

    reset.removeEventListener('click', this._handleInputReset);
  };

  /**
   * Event handlers
   */
  PredictiveSearchComponent.prototype._handleBodyMousedown = function(evt) {
    if (this.isResultVisible && this.nodes !== null) {
      if (
        evt.target.isEqualNode(this.nodes.input) ||
        this.nodes.input.contains(evt.target) ||
        evt.target.isEqualNode(this.nodes.result) ||
        this.nodes.result.contains(evt.target)
      ) {
        this._resultNodeClicked = true;
      } else {
        if (isFunction(this.callbacks.onBodyMousedown)) {
          var returnedValue = this.callbacks.onBodyMousedown(this.nodes);
          if (isBoolean(returnedValue) && returnedValue) {
            this.close();
          }
        } else {
          this.close();
        }
      }
    }
  };

  PredictiveSearchComponent.prototype._handleInputFocus = function(evt) {
    if (isFunction(this.callbacks.onInputFocus)) {
      var returnedValue = this.callbacks.onInputFocus(this.nodes);
      if (isBoolean(returnedValue) && !returnedValue) {
        return false;
      }
    }

    if (evt.target.value.length > 0) {
      this._search();
    }

    return true;
  };

  PredictiveSearchComponent.prototype._handleInputBlur = function() {
    // This has to be done async, to wait for the focus to be on the next
    // element and avoid closing the results.
    // Example: Going from the input to the reset button.
    setTimeout(
      function() {
        if (isFunction(this.callbacks.onInputBlur)) {
          var returnedValue = this.callbacks.onInputBlur(this.nodes);
          if (isBoolean(returnedValue) && !returnedValue) {
            return false;
          }
        }

        if (document.activeElement.isEqualNode(this.nodes.reset)) {
          return false;
        }

        if (this._resultNodeClicked) {
          this._resultNodeClicked = false;
          return false;
        }

        this.close();
      }.bind(this)
    );

    return true;
  };

  PredictiveSearchComponent.prototype._addAccessibilityAnnouncer = function() {
    this._accessibilityAnnouncerDiv = window.document.createElement('div');

    this._accessibilityAnnouncerDiv.setAttribute(
      'style',
      'position: absolute !important; overflow: hidden; clip: rect(0 0 0 0); height: 1px; width: 1px; margin: -1px; padding: 0; border: 0;'
    );

    this._accessibilityAnnouncerDiv.setAttribute('data-search-announcer', '');
    this._accessibilityAnnouncerDiv.setAttribute('aria-live', 'polite');
    this._accessibilityAnnouncerDiv.setAttribute('aria-atomic', 'true');

    this.nodes.result.parentElement.appendChild(
      this._accessibilityAnnouncerDiv
    );
  };

  PredictiveSearchComponent.prototype._removeAccessibilityAnnouncer = function() {
    this.nodes.result.parentElement.removeChild(
      this._accessibilityAnnouncerDiv
    );
  };

  PredictiveSearchComponent.prototype._updateAccessibilityAttributesAfterSelectingElement = function(
    previousSelectedElement,
    currentSelectedElement
  ) {
    // Update the active descendant on the search input
    this.nodes.input.setAttribute(
      'aria-activedescendant',
      currentSelectedElement.id
    );

    // Unmark the previousSelected elemented as selected
    if (previousSelectedElement) {
      previousSelectedElement.removeAttribute('aria-selected');
    }

    // Mark the element as selected
    currentSelectedElement.setAttribute('aria-selected', true);
  };

  PredictiveSearchComponent.prototype._clearAriaActiveDescendant = function() {
    this.nodes.input.setAttribute('aria-activedescendant', '');
  };

  PredictiveSearchComponent.prototype._announceNumberOfResultsFound = function(
    results
  ) {
    var currentAnnouncedMessage = this._accessibilityAnnouncerDiv.innerHTML;
    var newMessage = this.numberOfResultsTemplateFct(results);

    // If the messages are the same, they won't get announced
    // add white space so it gets announced
    if (currentAnnouncedMessage === newMessage) {
      newMessage = newMessage + '&nbsp;';
    }

    this._accessibilityAnnouncerDiv.innerHTML = newMessage;
  };

  PredictiveSearchComponent.prototype._announceLoadingState = function() {
    this._accessibilityAnnouncerDiv.innerHTML = this.loadingResultsMessageTemplateFct();
  };

  PredictiveSearchComponent.prototype._handleInputKeyup = function(evt) {
    var UP_ARROW_KEY_CODE = 38;
    var DOWN_ARROW_KEY_CODE = 40;
    var RETURN_KEY_CODE = 13;
    var ESCAPE_KEY_CODE = 27;

    if (isFunction(this.callbacks.onInputKeyup)) {
      var returnedValue = this.callbacks.onInputKeyup(this.nodes);
      if (isBoolean(returnedValue) && !returnedValue) {
        return false;
      }
    }

    this._toggleClearButtonVisibility();

    if (this.isResultVisible && this.nodes !== null) {
      if (evt.keyCode === UP_ARROW_KEY_CODE) {
        this._navigateOption(evt, 'UP');
        return true;
      }

      if (evt.keyCode === DOWN_ARROW_KEY_CODE) {
        this._navigateOption(evt, 'DOWN');
        return true;
      }

      if (evt.keyCode === RETURN_KEY_CODE) {
        this._selectOption();
        return true;
      }

      if (evt.keyCode === ESCAPE_KEY_CODE) {
        this.close();
      }
    }

    if (evt.target.value.length <= 0) {
      this.close();
      this._setKeyword('');
    } else if (evt.target.value.length > 0) {
      this._search();
    }

    return true;
  };

  PredictiveSearchComponent.prototype._handleInputKeydown = function(evt) {
    var RETURN_KEY_CODE = 13;
    var UP_ARROW_KEY_CODE = 38;
    var DOWN_ARROW_KEY_CODE = 40;

    // Prevent the form default submission if there is a selected option
    if (evt.keyCode === RETURN_KEY_CODE && this._getSelectedOption() !== null) {
      evt.preventDefault();
    }

    // Prevent the cursor from moving in the input when using the up and down arrow keys
    if (
      evt.keyCode === UP_ARROW_KEY_CODE ||
      evt.keyCode === DOWN_ARROW_KEY_CODE
    ) {
      evt.preventDefault();
    }
  };

  PredictiveSearchComponent.prototype._handleInputReset = function(evt) {
    evt.preventDefault();

    if (isFunction(this.callbacks.onInputReset)) {
      var returnedValue = this.callbacks.onInputReset(this.nodes);
      if (isBoolean(returnedValue) && !returnedValue) {
        return false;
      }
    }

    this.nodes.input.value = '';
    this.nodes.input.focus();
    this._toggleClearButtonVisibility();
    this.close();

    return true;
  };

  PredictiveSearchComponent.prototype._navigateOption = function(
    evt,
    direction
  ) {
    var currentOption = this._getSelectedOption();

    if (!currentOption) {
      var firstOption = this.nodes.result.querySelector(
        this.selectors.searchResult
      );
      firstOption.classList.add(this.classes.itemSelected);
      this._updateAccessibilityAttributesAfterSelectingElement(
        null,
        firstOption
      );
    } else {
      if (direction === 'DOWN') {
        var nextOption = currentOption.nextElementSibling;
        if (nextOption) {
          currentOption.classList.remove(this.classes.itemSelected);
          nextOption.classList.add(this.classes.itemSelected);
          this._updateAccessibilityAttributesAfterSelectingElement(
            currentOption,
            nextOption
          );
        }
      } else {
        var previousOption = currentOption.previousElementSibling;
        if (previousOption) {
          currentOption.classList.remove(this.classes.itemSelected);
          previousOption.classList.add(this.classes.itemSelected);
          this._updateAccessibilityAttributesAfterSelectingElement(
            currentOption,
            previousOption
          );
        }
      }
    }
  };

  PredictiveSearchComponent.prototype._getSelectedOption = function() {
    return this.nodes.result.querySelector('.' + this.classes.itemSelected);
  };

  PredictiveSearchComponent.prototype._selectOption = function() {
    var selectedOption = this._getSelectedOption();

    if (selectedOption) {
      selectedOption.querySelector('a, button').click();
    }
  };

  PredictiveSearchComponent.prototype._search = function() {
    var newSearchKeyword = this.nodes.input.value;

    if (this._searchKeyword === newSearchKeyword) {
      return;
    }

    clearTimeout(this._latencyTimer);
    this._latencyTimer = setTimeout(
      function() {
        this.results.isLoading = true;

        // Annonuce that we're loading the results
        this._announceLoadingState();

        this.nodes.result.classList.add(this.classes.visibleVariant);
        // NOTE: We could benifit in using DOMPurify.
        // https://github.com/cure53/DOMPurify
        this.nodes.result.innerHTML = this.resultTemplateFct(this.results);
      }.bind(this),
      500
    );

    this.predictiveSearch.query(newSearchKeyword);
    this._setKeyword(newSearchKeyword);
  };

  PredictiveSearchComponent.prototype._handlePredictiveSearchSuccess = function(
    json
  ) {
    clearTimeout(this._latencyTimer);
    this.results = json.resources.results;

    this.results.isLoading = false;
    this.results.products = this.results.products.slice(
      0,
      this.numberOfResults
    );
    this.results.canLoadMore =
      this.numberOfResults <= this.results.products.length;
    this.results.searchQuery = this.nodes.input.value;

    if (this.results.products.length > 0 || this.results.searchQuery) {
      this.nodes.result.innerHTML = this.resultTemplateFct(this.results);
      this._announceNumberOfResultsFound(this.results);
      this.open();
    } else {
      this.nodes.result.innerHTML = '';

      this._closeOnNoResults();
    }
  };

  PredictiveSearchComponent.prototype._handlePredictiveSearchError = function() {
    clearTimeout(this._latencyTimer);
    this.nodes.result.innerHTML = '';

    this._closeOnNoResults();
  };

  PredictiveSearchComponent.prototype._closeOnNoResults = function() {
    if (this.nodes) {
      this.nodes.result.classList.remove(this.classes.visibleVariant);
    }

    this.isResultVisible = false;
  };

  PredictiveSearchComponent.prototype._setKeyword = function(keyword) {
    this._searchKeyword = keyword;
  };

  PredictiveSearchComponent.prototype._toggleClearButtonVisibility = function() {
    if (!this.nodes.reset) {
      return;
    }

    if (this.nodes.input.value.length > 0) {
      this.nodes.reset.classList.add(this.classes.clearButtonVisible);
    } else {
      this.nodes.reset.classList.remove(this.classes.clearButtonVisible);
    }
  };

  /**
   * Public methods
   */
  PredictiveSearchComponent.prototype.open = function() {
    if (this.isResultVisible) {
      return;
    }

    if (isFunction(this.callbacks.onBeforeOpen)) {
      var returnedValue = this.callbacks.onBeforeOpen(this.nodes);
      if (isBoolean(returnedValue) && !returnedValue) {
        return false;
      }
    }

    this.nodes.result.classList.add(this.classes.visibleVariant);
    this.nodes.input.setAttribute('aria-expanded', true);
    this.isResultVisible = true;

    if (isFunction(this.callbacks.onOpen)) {
      return this.callbacks.onOpen(this.nodes) || true;
    }

    return true;
  };

  PredictiveSearchComponent.prototype.close = function() {
    if (!this.isResultVisible) {
      return true;
    }

    if (isFunction(this.callbacks.onBeforeClose)) {
      var returnedValue = this.callbacks.onBeforeClose(this.nodes);
      if (isBoolean(returnedValue) && !returnedValue) {
        return false;
      }
    }

    if (this.nodes) {
      this.nodes.result.classList.remove(this.classes.visibleVariant);
    }

    this.nodes.input.setAttribute('aria-expanded', false);
    this._clearAriaActiveDescendant();
    this._setKeyword('');

    if (isFunction(this.callbacks.onClose)) {
      this.callbacks.onClose(this.nodes);
    }

    this.isResultVisible = false;
    this.results = {};

    return true;
  };

  PredictiveSearchComponent.prototype.destroy = function() {
    this.close();

    if (isFunction(this.callbacks.onBeforeDestroy)) {
      var returnedValue = this.callbacks.onBeforeDestroy(this.nodes);
      if (isBoolean(returnedValue) && !returnedValue) {
        return false;
      }
    }

    this.nodes.result.classList.remove(this.classes.visibleVariant);
    removeInputAttributes(this.nodes.input);
    this._removeInputEventListeners();
    this._removeBodyEventListener();
    this._removeAccessibilityAnnouncer();
    this._removeClearButtonEventListener();

    if (isFunction(this.callbacks.onDestroy)) {
      this.callbacks.onDestroy(this.nodes);
    }

    return true;
  };

  PredictiveSearchComponent.prototype.clearAndClose = function() {
    this.nodes.input.value = '';
    this.close();
  };

  /**
   * Utilities
   */
  function getTypeOf(value) {
    return Object.prototype.toString.call(value);
  }

  function isString(value) {
    return getTypeOf(value) === '[object String]';
  }

  function isBoolean(value) {
    return getTypeOf(value) === '[object Boolean]';
  }

  function isFunction(value) {
    return getTypeOf(value) === '[object Function]';
  }

  return PredictiveSearchComponent;
})(Shopify.theme.PredictiveSearch);

theme.Sections.prototype = Object.assign({}, theme.Sections.prototype, {
  _createInstance: function(container, constructor) {
    var id = container.getAttribute('data-section-id');
    var type = container.getAttribute('data-section-type');

    constructor = constructor || this.constructors[type];

    if (typeof constructor === 'undefined') {
      return;
    }

    var instance = Object.assign(new constructor(container), {
      id: id,
      type: type,
      container: container
    });

    this.instances.push(instance);
  },

  _onSectionLoad: function(evt) {
    var container = document.querySelector(
      '[data-section-id="' + evt.detail.sectionId + '"]'
    );

    if (container) {
      this._createInstance(container);
    }
  },

  _onSectionUnload: function(evt) {
    this.instances = this.instances.filter(function(instance) {
      var isEventInstance = instance.id === evt.detail.sectionId;

      if (isEventInstance) {
        if (typeof instance.onUnload === 'function') {
          instance.onUnload(evt);
        }
      }

      return !isEventInstance;
    });
  },

  _onSelect: function(evt) {
    // eslint-disable-next-line no-shadow
    var instance = this.instances.find(function(instance) {
      return instance.id === evt.detail.sectionId;
    });

    if (
      typeof instance !== 'undefined' &&
      typeof instance.onSelect === 'function'
    ) {
      instance.onSelect(evt);
    }
  },

  _onDeselect: function(evt) {
    // eslint-disable-next-line no-shadow
    var instance = this.instances.find(function(instance) {
      return instance.id === evt.detail.sectionId;
    });

    if (
      typeof instance !== 'undefined' &&
      typeof instance.onDeselect === 'function'
    ) {
      instance.onDeselect(evt);
    }
  },

  _onBlockSelect: function(evt) {
    // eslint-disable-next-line no-shadow
    var instance = this.instances.find(function(instance) {
      return instance.id === evt.detail.sectionId;
    });

    if (
      typeof instance !== 'undefined' &&
      typeof instance.onBlockSelect === 'function'
    ) {
      instance.onBlockSelect(evt);
    }
  },

  _onBlockDeselect: function(evt) {
    // eslint-disable-next-line no-shadow
    var instance = this.instances.find(function(instance) {
      return instance.id === evt.detail.sectionId;
    });

    if (
      typeof instance !== 'undefined' &&
      typeof instance.onBlockDeselect === 'function'
    ) {
      instance.onBlockDeselect(evt);
    }
  },

  register: function(type, constructor) {
    this.constructors[type] = constructor;

    document.querySelectorAll('[data-section-type="' + type + '"]').forEach(
      function(container) {
        this._createInstance(container, constructor);
      }.bind(this)
    );
  }
});

theme.HeaderSection = (function() {
  function Header() {
    theme.Header.init();
    theme.MobileNav.init();
    theme.SearchDrawer.init();
    theme.Search.init();
  }

  Header.prototype = Object.assign({}, Header.prototype, {
    onUnload: function() {
      theme.Header.unload();
      theme.Search.unload();
      theme.MobileNav.unload();
    }
  });

  return Header;
})();


/* ================ GLOBAL ================ */
/*============================================================================
  Drawer modules
==============================================================================*/
theme.Drawers = (function() {
  function Drawer(id, position, options) {
    var DEFAULT_OPEN_CLASS = 'js-drawer-open';
    var DEFAULT_CLOSE_CLASS = 'js-drawer-close';

    var defaults = {
      selectors: {
        openVariant: '.' + DEFAULT_OPEN_CLASS + '-' + position,
        close: '.' + DEFAULT_CLOSE_CLASS
      },
      classes: {
        open: DEFAULT_OPEN_CLASS,
        openVariant: DEFAULT_OPEN_CLASS + '-' + position
      },
      withPredictiveSearch: false
    };

    this.nodes = {
      parents: [document.documentElement, document.body],
      page: document.getElementById('PageContainer')
    };

    this.eventHandlers = {};

    this.config = Object.assign({}, defaults, options);
    this.position = position;
    this.drawer = document.getElementById(id);

    if (!this.drawer) {
      return false;
    }

    this.drawerIsOpen = false;
    this.init();
  }

  Drawer.prototype.init = function() {
    document
      .querySelector(this.config.selectors.openVariant)
      .addEventListener('click', this.open.bind(this));
    this.drawer
      .querySelector(this.config.selectors.close)
      .addEventListener('click', this.close.bind(this));
  };

  Drawer.prototype.open = function(evt) {
    // Keep track if drawer was opened from a click, or called by another function
    var externalCall = false;

    // Prevent following href if link is clicked
    if (evt) {
      evt.preventDefault();
    } else {
      externalCall = true;
    }

    // Without this, the drawer opens, the click event bubbles up to nodes.page
    // which closes the drawer.
    if (evt && evt.stopPropagation) {
      evt.stopPropagation();
      // save the source of the click, we'll focus to this on close
      this.activeSource = evt.currentTarget;
    }

    if (this.drawerIsOpen && !externalCall) {
      return this.close();
    }

    // Add is-transitioning class to moved elements on open so drawer can have
    // transition for close animation
    if (!this.config.withPredictiveSearch) {
      theme.Helpers.prepareTransition(this.drawer);
    }

    this.nodes.parents.forEach(
      function(parent) {
        parent.classList.add(
          this.config.classes.open,
          this.config.classes.openVariant
        );
      }.bind(this)
    );

    this.drawerIsOpen = true;

    // Run function when draw opens if set
    if (
      this.config.onDrawerOpen &&
      typeof this.config.onDrawerOpen === 'function'
    ) {
      if (!externalCall) {
        this.config.onDrawerOpen();
      }
    }

    if (this.activeSource && this.activeSource.hasAttribute('aria-expanded')) {
      this.activeSource.setAttribute('aria-expanded', 'true');
    }

    // Set focus on drawer
    var trapFocusConfig = {
      container: this.drawer
    };

    if (this.config.elementToFocusOnOpen) {
      trapFocusConfig.elementToFocus = this.config.elementToFocusOnOpen;
    }

    slate.a11y.trapFocus(trapFocusConfig);

    this.bindEvents();

    return this;
  };

  Drawer.prototype.close = function() {
    if (!this.drawerIsOpen) {
      // don't close a closed drawer
      return;
    }

    // deselect any focused form elements
    document.activeElement.dispatchEvent(
      new CustomEvent('blur', { bubbles: true, cancelable: true })
    );

    // Ensure closing transition is applied to moved elements, like the nav
    if (!this.config.withPredictiveSearch) {
      theme.Helpers.prepareTransition(this.drawer);
    }

    this.nodes.parents.forEach(
      function(parent) {
        parent.classList.remove(
          this.config.classes.open,
          this.config.classes.openVariant
        );
      }.bind(this)
    );

    if (this.activeSource && this.activeSource.hasAttribute('aria-expanded')) {
      this.activeSource.setAttribute('aria-expanded', 'false');
    }

    this.drawerIsOpen = false;

    // Remove focus on drawer
    slate.a11y.removeTrapFocus({
      container: this.drawer
    });

    this.unbindEvents();

    // Run function when draw closes if set
    if (
      this.config.onDrawerClose &&
      typeof this.config.onDrawerClose === 'function'
    ) {
      this.config.onDrawerClose();
    }
  };

  Drawer.prototype.bindEvents = function() {
    this.eventHandlers.drawerKeyupHandler = function(evt) {
      // close on 'esc' keypress
      if (evt.keyCode === 27) {
        this.close();
        return false;
      } else {
        return true;
      }
    }.bind(this);

    this.eventHandlers.drawerTouchmoveHandler = function() {
      return false;
    };

    this.eventHandlers.drawerClickHandler = function() {
      this.close();
      return false;
    }.bind(this);

    // Add event listener to document body
    document.body.addEventListener(
      'keyup',
      this.eventHandlers.drawerKeyupHandler
    );

    // Lock scrolling on mobile
    this.nodes.page.addEventListener(
      'touchmove',
      this.eventHandlers.drawerTouchmoveHandler
    );

    this.nodes.page.addEventListener(
      'click',
      this.eventHandlers.drawerClickHandler
    );
  };

  Drawer.prototype.unbindEvents = function() {
    this.nodes.page.removeEventListener(
      'touchmove',
      this.eventHandlers.drawerTouchmoveHandler
    );
    this.nodes.page.removeEventListener(
      'click',
      this.eventHandlers.drawerClickHandler
    );
    document.body.removeEventListener(
      'keyup',
      this.eventHandlers.drawerKeyupHandler
    );
  };

  return Drawer;
})();

theme.Helpers = (function() {
  var touchDevice = false;

  var classes = {
    preventScrolling: 'prevent-scrolling'
  };

  var scrollPosition = window.pageYOffset;

  function setTouch() {
    touchDevice = true;
  }

  function isTouch() {
    return touchDevice;
  }

  function enableScrollLock() {
    scrollPosition = window.pageYOffset;
    document.body.style.top = '-' + scrollPosition + 'px';
    document.body.classList.add(classes.preventScrolling);
  }

  function disableScrollLock() {
    document.body.classList.remove(classes.preventScrolling);
    document.body.style.removeProperty('top');
    window.scrollTo(0, scrollPosition);
  }

  function debounce(func, wait, immediate) {
    var timeout;

    return function() {
      var context = this,
        args = arguments;

      var later = function() {
        timeout = null;
        if (!immediate) func.apply(context, args);
      };

      var callNow = immediate && !timeout;
      clearTimeout(timeout);
      timeout = setTimeout(later, wait);
      if (callNow) func.apply(context, args);
    };
  }

  function getScript(source, beforeEl) {
    return new Promise(function(resolve, reject) {
      var script = document.createElement('script');
      var prior = beforeEl || document.getElementsByTagName('script')[0];

      script.async = true;
      script.defer = true;

      // eslint-disable-next-line shopify/prefer-early-return
      function onloadHander(_, isAbort) {
        if (
          isAbort ||
          !script.readyState ||
          /loaded|complete/.test(script.readyState)
        ) {
          script.onload = null;
          script.onreadystatechange = null;
          script = undefined;

          if (isAbort) {
            reject();
          } else {
            resolve();
          }
        }
      }

      script.onload = onloadHander;
      script.onreadystatechange = onloadHander;

      script.src = source;
      prior.parentNode.insertBefore(script, prior);
    });
  }

  /* Based on the prepareTransition by Jonathan Snook */
  /* Jonathan Snook - MIT License - https://github.com/snookca/prepareTransition */
  function prepareTransition(element) {
    element.addEventListener(
      'transitionend',
      function(event) {
        event.currentTarget.classList.remove('is-transitioning');
      },
      { once: true }
    );

    var properties = [
      'transition-duration',
      '-moz-transition-duration',
      '-webkit-transition-duration',
      '-o-transition-duration'
    ];

    var duration = 0;

    properties.forEach(function(property) {
      var computedValue = getComputedStyle(element)[property];

      if (computedValue) {
        computedValue.replace(/\D/g, '');
        duration || (duration = parseFloat(computedValue));
      }
    });

    if (duration !== 0) {
      element.classList.add('is-transitioning');
      element.offsetWidth;
    }
  }

  /*!
   * Serialize all form data into a SearchParams string
   * (c) 2020 Chris Ferdinandi, MIT License, https://gomakethings.com
   * @param  {Node}   form The form to serialize
   * @return {String}      The serialized form data
   */
  function serialize(form) {
    var arr = [];
    Array.prototype.slice.call(form.elements).forEach(function(field) {
      if (
        !field.name ||
        field.disabled ||
        ['file', 'reset', 'submit', 'button'].indexOf(field.type) > -1
      )
        return;
      if (field.type === 'select-multiple') {
        Array.prototype.slice.call(field.options).forEach(function(option) {
          if (!option.selected) return;
          arr.push(
            encodeURIComponent(field.name) +
              '=' +
              encodeURIComponent(option.value)
          );
        });
        return;
      }
      if (['checkbox', 'radio'].indexOf(field.type) > -1 && !field.checked)
        return;
      arr.push(
        encodeURIComponent(field.name) + '=' + encodeURIComponent(field.value)
      );
    });
    return arr.join('&');
  }
  function cookiesEnabled() {
    var cookieEnabled = navigator.cookieEnabled;

    if (!cookieEnabled) {
      document.cookie = 'testcookie';
      cookieEnabled = document.cookie.indexOf('testcookie') !== -1;
    }

    return cookieEnabled;
  }

  function promiseStylesheet(stylesheet) {
    var stylesheetUrl = stylesheet || theme.stylesheet;

    if (typeof this.stylesheetPromise === 'undefined') {
      this.stylesheetPromise = new Promise(function(resolve) {
        var link = document.querySelector('link[href="' + stylesheetUrl + '"]');

        if (link.loaded) resolve();

        link.addEventListener('load', function() {
          setTimeout(resolve, 0);
        });
      });
    }

    return this.stylesheetPromise;
  }

  return {
    setTouch: setTouch,
    isTouch: isTouch,
    enableScrollLock: enableScrollLock,
    disableScrollLock: disableScrollLock,
    debounce: debounce,
    getScript: getScript,
    prepareTransition: prepareTransition,
    serialize: serialize,
    cookiesEnabled: cookiesEnabled,
    promiseStylesheet: promiseStylesheet
  };
})();

(function() {
  // (a11y) This function will be used by the Predictive Search Component
  // to announce the number of search results
  function numberOfResultsTemplateFct(data) {
    if (data.products.length === 1) {
      return theme.strings.one_result_found;
    } else {
      return theme.strings.number_of_results_found.replace(
        '[results_count]',
        data.products.length
      );
    }
  }

  // (a11y) This function will be used by the Predictive Search Component
  // to announce that it's loading results
  function loadingResultsMessageTemplateFct() {
    return theme.strings.loading;
  }

  function isPredictiveSearchSupported() {
    var shopifyFeatures = JSON.parse(
      document.getElementById('shopify-features').textContent
    );

    return shopifyFeatures.predictiveSearch;
  }

  function isPredictiveSearchEnabled() {
    return window.theme.settings.predictiveSearchEnabled;
  }

  function canInitializePredictiveSearch() {
    return isPredictiveSearchSupported() && isPredictiveSearchEnabled();
  }

  // listen for search submits and validate query
  function validateSearchHandler(searchEl, submitEl) {
    submitEl.addEventListener(
      'click',
      validateSearchInput.bind(this, searchEl)
    );
  }

  // if there is nothing in the search field, prevent submit
  function validateSearchInput(searchEl, evt) {
    var isInputValueEmpty = searchEl.value.trim().length === 0;
    if (!isInputValueEmpty) {
      return;
    }

    if (typeof evt !== 'undefined') {
      evt.preventDefault();
    }

    searchEl.focus();
  }

  window.theme.SearchPage = (function() {
    var selectors = {
      searchReset: '[data-search-page-predictive-search-clear]',
      searchInput: '[data-search-page-predictive-search-input]',
      searchSubmit: '[data-search-page-predictive-search-submit]',
      searchResults: '[data-predictive-search-mount="default"]'
    };

    var componentInstance;

    function init(config) {
      var searchInput = document.querySelector(selectors.searchInput);
      var searchSubmit = document.querySelector(selectors.searchSubmit);
      var searchUrl = searchInput.dataset.baseUrl;

      componentInstance = new window.Shopify.theme.PredictiveSearchComponent({
        selectors: {
          input: selectors.searchInput,
          reset: selectors.searchReset,
          result: selectors.searchResults
        },
        searchUrl: searchUrl,
        resultTemplateFct: window.theme.SearchResultsTemplate,
        numberOfResultsTemplateFct: numberOfResultsTemplateFct,
        loadingResultsMessageTemplateFct: loadingResultsMessageTemplateFct,
        onOpen: function(nodes) {
          if (config.isTabletAndUp) {
            return;
          }

          var searchInputBoundingRect = searchInput.getBoundingClientRect();
          var bodyHeight = document.body.offsetHeight;
          var offset = 50;
          var resultsMaxHeight =
            bodyHeight - searchInputBoundingRect.bottom - offset;

          nodes.result.style.maxHeight = resultsMaxHeight + 'px';
        },
        onBeforeDestroy: function(nodes) {
          // If the viewport width changes from mobile to tablet
          // reset the top position of the results
          nodes.result.style.maxHeight = '';
        }
      });

      validateSearchHandler(searchInput, searchSubmit);
    }

    function unload() {
      if (!componentInstance) {
        return;
      }
      componentInstance.destroy();
      componentInstance = null;
    }

    return {
      init: init,
      unload: unload
    };
  })();

  window.theme.SearchHeader = (function() {
    var selectors = {
      searchInput: '[data-predictive-search-drawer-input]',
      searchResults: '[data-predictive-search-mount="drawer"]',
      searchFormContainer: '[data-search-form-container]',
      searchSubmit: '[data-search-form-submit]'
    };

    var componentInstance;

    function init(config) {
      var searchInput = document.querySelector(selectors.searchInput);
      var searchSubmit = document.querySelector(selectors.searchSubmit);
      var searchUrl = searchInput.dataset.baseUrl;

      componentInstance = new window.Shopify.theme.PredictiveSearchComponent({
        selectors: {
          input: selectors.searchInput,
          result: selectors.searchResults
        },
        searchUrl: searchUrl,
        resultTemplateFct: window.theme.SearchResultsTemplate,
        numberOfResultsTemplateFct: numberOfResultsTemplateFct,
        numberOfResults: config.numberOfResults,
        loadingResultsMessageTemplateFct: loadingResultsMessageTemplateFct,
        onInputBlur: function() {
          return false;
        },
        onOpen: function(nodes) {
          var searchInputBoundingRect = searchInput.getBoundingClientRect();

          // For tablet screens and up, stop the scroll area from extending past
          // the bottom of the screen because we're locking the body scroll
          var maxHeight =
            window.innerHeight -
            searchInputBoundingRect.bottom -
            (config.isTabletAndUp ? 20 : 0);

          nodes.result.style.top = config.isTabletAndUp
            ? ''
            : searchInputBoundingRect.bottom + 'px';
          nodes.result.style.maxHeight = maxHeight + 'px';
        },
        onClose: function(nodes) {
          nodes.result.style.maxHeight = '';
        },
        onBeforeDestroy: function(nodes) {
          // If the viewport width changes from mobile to tablet
          // reset the top position of the results
          nodes.result.style.top = '';
        }
      });

      validateSearchHandler(searchInput, searchSubmit);
    }

    function unload() {
      if (!componentInstance) {
        return;
      }

      componentInstance.destroy();
      componentInstance = null;
    }

    function clearAndClose() {
      if (!componentInstance) {
        return;
      }

      componentInstance.clearAndClose();
    }

    return {
      init: init,
      unload: unload,
      clearAndClose: clearAndClose
    };
  })();

  window.theme.Search = (function() {
    var classes = {
      searchTemplate: 'template-search'
    };
    var selectors = {
      siteHeader: '.site-header'
    };
    var mediaQueryList = {
      mobile: window.matchMedia('(max-width: 749px)'),
      tabletAndUp: window.matchMedia('(min-width: 750px)')
    };

    function init() {
      if (!document.querySelector(selectors.siteHeader)) {
        return;
      }

      if (!canInitializePredictiveSearch()) {
        return;
      }

      Object.keys(mediaQueryList).forEach(function(device) {
        mediaQueryList[device].addListener(initSearchAccordingToViewport);
      });

      initSearchAccordingToViewport();
    }

    function initSearchAccordingToViewport() {
      theme.SearchDrawer.close();
      theme.SearchHeader.unload();
      theme.SearchPage.unload();

      if (mediaQueryList.mobile.matches) {
        theme.SearchHeader.init({
          numberOfResults: 4,
          isTabletAndUp: false
        });

        if (isSearchPage()) {
          theme.SearchPage.init({ isTabletAndUp: false });
        }
      } else {
        // Tablet and up
        theme.SearchHeader.init({
          numberOfResults: 4,
          isTabletAndUp: true
        });

        if (isSearchPage()) {
          theme.SearchPage.init({ isTabletAndUp: true });
        }
      }
    }

    function isSearchPage() {
      return document.body.classList.contains(classes.searchTemplate);
    }

    function unload() {
      theme.SearchHeader.unload();
      theme.SearchPage.unload();
    }

    return {
      init: init,
      unload: unload
    };
  })();
})();

theme.SearchDrawer = (function() {
  var selectors = {
    headerSection: '[data-header-section]',
    drawer: '[data-predictive-search-drawer]',
    drawerOpenButton: '[data-predictive-search-open-drawer]',
    headerSearchInput: '[data-predictive-search-drawer-input]',
    predictiveSearchWrapper: '[data-predictive-search-mount="drawer"]'
  };

  var drawerInstance;

  function init() {
    setAccessibilityProps();

    drawerInstance = new theme.Drawers('SearchDrawer', 'top', {
      onDrawerOpen: function() {
        setHeight();
        theme.MobileNav.closeMobileNav();
        lockBodyScroll();
      },
      onDrawerClose: function() {
        theme.SearchHeader.clearAndClose();
        var drawerOpenButton = document.querySelector(
          selectors.drawerOpenButton
        );

        if (drawerOpenButton) drawerOpenButton.focus();

        unlockBodyScroll();
      },
      withPredictiveSearch: true,
      elementToFocusOnOpen: document.querySelector(selectors.headerSearchInput)
    });
  }

  function setAccessibilityProps() {
    var drawerOpenButton = document.querySelector(selectors.drawerOpenButton);

    if (drawerOpenButton) {
      drawerOpenButton.setAttribute('aria-controls', 'SearchDrawer');
      drawerOpenButton.setAttribute('aria-expanded', 'false');
      drawerOpenButton.setAttribute('aria-controls', 'dialog');
    }
  }

  function setHeight() {
    var searchDrawer = document.querySelector(selectors.drawer);
    var headerHeight = document.querySelector(selectors.headerSection)
      .offsetHeight;

    searchDrawer.style.height = headerHeight + 'px';
  }

  function close() {
    drawerInstance.close();
  }

  function lockBodyScroll() {
    theme.Helpers.enableScrollLock();
  }

  function unlockBodyScroll() {
    theme.Helpers.disableScrollLock();
  }

  return {
    init: init,
    close: close
  };
})();

theme.Header = (function() {
  var selectors = {
    body: 'body',
    navigation: '#AccessibleNav',
    siteNavHasDropdown: '[data-has-dropdowns]',
    siteNavChildLinks: '.site-nav__child-link',
    siteNavActiveDropdown: '.site-nav--active-dropdown',
    siteNavHasCenteredDropdown: '.site-nav--has-centered-dropdown',
    siteNavCenteredDropdown: '.site-nav__dropdown--centered',
    siteNavLinkMain: '.site-nav__link--main',
    siteNavChildLink: '.site-nav__link--last',
    siteNavDropdown: '.site-nav__dropdown',
    siteHeader: '.site-header'
  };

  var config = {
    activeClass: 'site-nav--active-dropdown',
    childLinkClass: 'site-nav__child-link',
    rightDropdownClass: 'site-nav__dropdown--right',
    leftDropdownClass: 'site-nav__dropdown--left'
  };

  var cache = {};

  function init() {
    cacheSelectors();
    styleDropdowns(document.querySelectorAll(selectors.siteNavHasDropdown));
    positionFullWidthDropdowns();

    cache.parents.forEach(function(element) {
      element.addEventListener('click', submenuParentClickHandler);
    });

    // check when we're leaving a dropdown and close the active dropdown
    cache.siteNavChildLink.forEach(function(element) {
      element.addEventListener('focusout', submenuFocusoutHandler);
    });

    cache.topLevel.forEach(function(element) {
      element.addEventListener('focus', hideDropdown);
    });

    cache.subMenuLinks.forEach(function(element) {
      element.addEventListener('click', stopImmediatePropagation);
    });

    window.addEventListener('resize', resizeHandler);
  }

  function stopImmediatePropagation(event) {
    event.stopImmediatePropagation();
  }

  function cacheSelectors() {
    var navigation = document.querySelector(selectors.navigation);

    cache = {
      nav: navigation,
      topLevel: document.querySelectorAll(selectors.siteNavLinkMain),
      parents: navigation.querySelectorAll(selectors.siteNavHasDropdown),
      subMenuLinks: document.querySelectorAll(selectors.siteNavChildLinks),
      activeDropdown: document.querySelector(selectors.siteNavActiveDropdown),
      siteHeader: document.querySelector(selectors.siteHeader),
      siteNavChildLink: document.querySelectorAll(selectors.siteNavChildLink)
    };
  }

  function showDropdown(element) {
    element.classList.add(config.activeClass);

    if (cache.activeDropdown) hideDropdown();

    cache.activeDropdown = element;

    element
      .querySelector(selectors.siteNavLinkMain)
      .setAttribute('aria-expanded', 'true');

    setTimeout(function() {
      window.addEventListener('keyup', keyUpHandler);
      document.body.addEventListener('click', hideDropdown);
    }, 250);
  }

  function hideDropdown() {
    if (!cache.activeDropdown) return;

    cache.activeDropdown
      .querySelector(selectors.siteNavLinkMain)
      .setAttribute('aria-expanded', 'false');
    cache.activeDropdown.classList.remove(config.activeClass);

    cache.activeDropdown = document.querySelector(
      selectors.siteNavActiveDropdown
    );

    window.removeEventListener('keyup', keyUpHandler);
    document.body.removeEventListener('click', hideDropdown);
  }

  function styleDropdowns(dropdownListItems) {
    dropdownListItems.forEach(function(item) {
      var dropdownLi = item.querySelector(selectors.siteNavDropdown);

      if (!dropdownLi) return;

      if (isRightOfLogo(item)) {
        dropdownLi.classList.remove(config.leftDropdownClass);
        dropdownLi.classList.add(config.rightDropdownClass);
      } else {
        dropdownLi.classList.remove(config.rightDropdownClass);
        dropdownLi.classList.add(config.leftDropdownClass);
      }
    });
  }

  function isRightOfLogo(item) {
    var rect = item.getBoundingClientRect();
    var win = item.ownerDocument.defaultView;
    var leftOffset = rect.left + win.pageXOffset;

    var headerWidth = Math.floor(cache.siteHeader.offsetWidth) / 2;
    return leftOffset > headerWidth;
  }

  function positionFullWidthDropdowns() {
    document
      .querySelectorAll(selectors.siteNavHasCenteredDropdown)
      .forEach(function(el) {
        var fullWidthDropdown = el.querySelector(
          selectors.siteNavCenteredDropdown
        );

        var fullWidthDropdownOffset = el.offsetTop + 41;
        fullWidthDropdown.style.top = fullWidthDropdownOffset + 'px';
      });
  }

  function keyUpHandler(event) {
    if (event.keyCode === 27) hideDropdown();
  }

  function resizeHandler() {
    adjustStyleAndPosition();
  }

  function submenuParentClickHandler(event) {
    var element = event.currentTarget;

    element.classList.contains(config.activeClass)
      ? hideDropdown()
      : showDropdown(element);
  }

  function submenuFocusoutHandler() {
    setTimeout(function() {
      if (
        document.activeElement.classList.contains(config.childLinkClass) ||
        !cache.activeDropdown
      ) {
        return;
      }

      hideDropdown();
    });
  }

  var adjustStyleAndPosition = theme.Helpers.debounce(function() {
    styleDropdowns(document.querySelectorAll(selectors.siteNavHasDropdown));
    positionFullWidthDropdowns();
  }, 50);

  function unload() {
    cache.topLevel.forEach(function(element) {
      element.removeEventListener('focus', hideDropdown);
    });

    cache.subMenuLinks.forEach(function(element) {
      element.removeEventListener('click', stopImmediatePropagation);
    });

    cache.parents.forEach(function(element) {
      element.removeEventListener('click', submenuParentClickHandler);
    });

    cache.siteNavChildLink.forEach(function(element) {
      element.removeEventListener('focusout', submenuFocusoutHandler);
    });

    window.removeEventListener('resize', resizeHandler);
    window.removeEventListener('keyup', keyUpHandler);
    document.body.removeEventListener('click', hideDropdown);
  }

  return {
    init: init,
    unload: unload
  };
})();

theme.MobileNav = (function() {
  var classes = {
    mobileNavOpenIcon: 'mobile-nav--open',
    mobileNavCloseIcon: 'mobile-nav--close',
    navLinkWrapper: 'mobile-nav__item',
    navLink: 'mobile-nav__link',
    subNavLink: 'mobile-nav__sublist-link',
    return: 'mobile-nav__return-btn',
    subNavActive: 'is-active',
    subNavClosing: 'is-closing',
    navOpen: 'js-menu--is-open',
    subNavShowing: 'sub-nav--is-open',
    thirdNavShowing: 'third-nav--is-open',
    subNavToggleBtn: 'js-toggle-submenu'
  };

  var cache = {};
  var isTransitioning;
  var activeSubNav;
  var activeTrigger;
  var menuLevel = 1;
  var mediumUpQuery = '(min-width: ' + theme.breakpoints.medium + 'px)';
  var mql = window.matchMedia(mediumUpQuery);

  function init() {
    cacheSelectors();

    if (cache.mobileNavToggle) {
      cache.mobileNavToggle.addEventListener('click', toggleMobileNav);
    }

    cache.subNavToggleBtns.forEach(function(element) {
      element.addEventListener('click', toggleSubNav);
    });

    mql.addListener(initBreakpoints);
  }

  function initBreakpoints() {
    if (
      mql.matches &&
      cache.mobileNavContainer.classList.contains(classes.navOpen)
    ) {
      closeMobileNav();
    }
  }

  function toggleMobileNav() {
    var mobileNavIsOpen = cache.mobileNavToggle.classList.contains(
      classes.mobileNavCloseIcon
    );

    if (mobileNavIsOpen) {
      closeMobileNav();
    } else {
      openMobileNav();
    }
  }

  function cacheSelectors() {
    cache = {
      pageContainer: document.querySelector('#PageContainer'),
      siteHeader: document.querySelector('.site-header'),
      mobileNavToggle: document.querySelector('.js-mobile-nav-toggle'),
      mobileNavContainer: document.querySelector('.mobile-nav-wrapper'),
      mobileNav: document.querySelector('#MobileNav'),
      sectionHeader: document.querySelector('#shopify-section-header'),
      subNavToggleBtns: document.querySelectorAll('.' + classes.subNavToggleBtn)
    };
  }

  function openMobileNav() {
    var translateHeaderHeight = cache.siteHeader.offsetHeight;

    theme.Helpers.prepareTransition(cache.mobileNavContainer);
    cache.mobileNavContainer.classList.add(classes.navOpen);

    cache.mobileNavContainer.style.transform =
      'translateY(' + translateHeaderHeight + 'px)';

    cache.pageContainer.style.transform =
      'translate3d(0, ' + cache.mobileNavContainer.scrollHeight + 'px, 0)';

    slate.a11y.trapFocus({
      container: cache.sectionHeader,
      elementToFocus: cache.mobileNavToggle
    });

    cache.mobileNavToggle.classList.add(classes.mobileNavCloseIcon);
    cache.mobileNavToggle.classList.remove(classes.mobileNavOpenIcon);
    cache.mobileNavToggle.setAttribute('aria-expanded', true);

    window.addEventListener('keyup', keyUpHandler);
  }

  function keyUpHandler(event) {
    if (event.which === 27) {
      closeMobileNav();
    }
  }

  function closeMobileNav() {
    theme.Helpers.prepareTransition(cache.mobileNavContainer);
    cache.mobileNavContainer.classList.remove(classes.navOpen);
    cache.mobileNavContainer.style.transform = 'translateY(-100%)';
    cache.pageContainer.setAttribute('style', '');

    slate.a11y.trapFocus({
      container: document.querySelector('html'),
      elementToFocus: document.body
    });

    cache.mobileNavContainer.addEventListener(
      'transitionend',
      mobileNavRemoveTrapFocus,
      { once: true }
    );

    cache.mobileNavToggle.classList.add(classes.mobileNavOpenIcon);
    cache.mobileNavToggle.classList.remove(classes.mobileNavCloseIcon);
    cache.mobileNavToggle.setAttribute('aria-expanded', false);
    cache.mobileNavToggle.focus();

    window.removeEventListener('keyup', keyUpHandler);
    window.scrollTo(0, 0);
  }

  function mobileNavRemoveTrapFocus() {
    slate.a11y.removeTrapFocus({
      container: cache.mobileNav
    });
  }

  function toggleSubNav(event) {
    if (isTransitioning) return;

    var toggleBtn = event.currentTarget;
    var isReturn = toggleBtn.classList.contains(classes.return);

    isTransitioning = true;

    if (isReturn) {
      var subNavToggleBtn = document.querySelectorAll(
        '.' + classes.subNavToggleBtn + "[data-level='" + (menuLevel - 1) + "']"
      );

      subNavToggleBtn.forEach(function(element) {
        element.classList.remove(classes.subNavActive);
      });

      if (activeTrigger) {
        activeTrigger.classList.remove(classes.subNavActive);
      }
    } else {
      toggleBtn.classList.add(classes.subNavActive);
    }

    activeTrigger = toggleBtn;

    goToSubnav(toggleBtn.getAttribute('data-target'));
  }

  function goToSubnav(target) {
    var targetMenu = target
      ? document.querySelector(
          '.mobile-nav__dropdown[data-parent="' + target + '"]'
        )
      : cache.mobileNav;

    menuLevel = targetMenu.dataset.level ? Number(targetMenu.dataset.level) : 1;

    if (activeSubNav) {
      theme.Helpers.prepareTransition(activeSubNav);
      activeSubNav.classList.add(classes.subNavClosing);
    }

    activeSubNav = targetMenu;

    var translateMenuHeight = targetMenu.offsetHeight;

    var openNavClass =
      menuLevel > 2 ? classes.thirdNavShowing : classes.subNavShowing;

    cache.mobileNavContainer.style.height = translateMenuHeight + 'px';
    cache.mobileNavContainer.classList.remove(classes.thirdNavShowing);
    cache.mobileNavContainer.classList.add(openNavClass);

    if (!target) {
      cache.mobileNavContainer.classList.remove(
        classes.thirdNavShowing,
        classes.subNavShowing
      );
    }

    /* if going back to first subnav, focus is on whole header */
    var container = menuLevel === 1 ? cache.sectionHeader : targetMenu;

    cache.mobileNavContainer.addEventListener(
      'transitionend',
      trapMobileNavFocus,
      { once: true }
    );

    function trapMobileNavFocus() {
      slate.a11y.trapFocus({
        container: container
      });

      cache.mobileNavContainer.removeEventListener(
        'transitionend',
        trapMobileNavFocus
      );

      isTransitioning = false;
    }

    // Match height of subnav
    cache.pageContainer.style.transform =
      'translateY(' + translateMenuHeight + 'px)';

    activeSubNav.classList.remove(classes.subNavClosing);
  }

  function unload() {
    mql.removeListener(initBreakpoints);
  }

  return {
    init: init,
    unload: unload,
    closeMobileNav: closeMobileNav
  };
})();

document.addEventListener('DOMContentLoaded', function() {
  var sections = new theme.Sections();

  sections.register('header-section', theme.HeaderSection);
});
